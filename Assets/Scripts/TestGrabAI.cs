﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animation))]
public class TestGrabAI : MonoBehaviour {
	// Use this for initialization
	private LineRenderer renderer;
	void Start () {
		renderer = this.gameObject.AddComponent<LineRenderer>();
		
		renderer.material = new Material(Shader.Find("Particles/Additive"));
		renderer.SetWidth(0.3f, 1f);
		renderer.SetColors(Color.cyan, Color.magenta);
		renderer.SetVertexCount(2);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void tryToGrab(Vector3 location){
		Transform t = this.transform.Find("Armature/Hip/Chest/Shoulder/UpperRightArm/LowerRightArm");
		Vector3 a = new Vector3(location.x - this.transform.position.x, 
		                        location.y - this.transform.position.y,
		                        location.z - this.transform.position.z);
		Vector3 b= this.transform.position;
		b.y += 10;

		Vector3 invPos = t.InverseTransformPoint(a);
		AnimationCurve curveX = AnimationCurve.Linear(0, 0, 1, invPos.x);
		AnimationCurve curveY = AnimationCurve.Linear(0, 0, 1, invPos.y);
		AnimationCurve curveZ = AnimationCurve.Linear(0, 0, 1, invPos.z);

		AnimationClip clip = new AnimationClip();

		renderer.SetPosition(0, b);
		renderer.SetPosition(1, b + a);

		//Armature/Hip/Chest/Shoulder/UpperRightArm/LowerRightArm
		clip.SetCurve("Armature/Hip/Chest/Shoulder/UpperRightArm/LowerRightArm", typeof(Transform), "localPosition.x", curveX);
		clip.SetCurve("Armature/Hip/Chest/Shoulder/UpperRightArm/LowerRightArm", typeof(Transform), "localPosition.y", curveY);
		clip.SetCurve("Armature/Hip/Chest/Shoulder/UpperRightArm/LowerRightArm", typeof(Transform), "localPosition.z", curveZ);

		GetComponent<Animation>().AddClip(clip, "grab");
		GetComponent<Animation>().Play("grab");
	}
}
