﻿using UnityEngine;
using System.Collections;

public class Hook : MonoBehaviour {
	private MG launcher;
	private GameObject hit;

	public bool fired = false;
	public bool hooked = false;
	public Vector3 normal = Vector3.zero;
	public Joint joint;

	public Hook(MG launcher){
		this.launcher = launcher;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (!this.hooked && this.fired){
			if (!MathUtils.isInsideSphere(this.transform.position, launcher.transform.position, launcher.ropeLength)){
				this.launcher.hookOutOfRange();
			}
		}	
	}

	public void setLauncher(MG launcher){
		this.launcher = launcher;
	}

	public void OnCollisionEnter(Collision collision){
		if (this.fired && !this.hooked && collision.gameObject.tag != "Champz" && this.hit == null){
			Vector3 scale = this.transform.localScale;
			DebugMenu.log("Hook hit something!.");
			this.joint = this.gameObject.AddComponent<FixedJoint>();
			this.hit = collision.gameObject;

			this.GetComponent<Rigidbody>().velocity = Vector3.zero;
			//this.transform.parent = collision.transform;
			this.normal = Vector3.up;
			this.launcher.hookHit();
			//remove the don't go through things script
			GameObject.Destroy(this.gameObject.GetComponent<DontGoThroughThings>());
			//remove the rigidbody so that it doesn't fall and attach this object to the one hit
			//this.transform.parent = this.hit.transform;
			this.joint.transform.parent = this.hit.transform;

			scale.x = scale.x / hit.transform.localScale.x;
			scale.y = scale.y / hit.transform.localScale.y;
			scale.z = scale.z / hit.transform.localScale.z;

			this.transform.localScale = scale;

			this.GetComponent<Rigidbody>().isKinematic = true;
		}
	}

	public void unhook(){
		Destroy(this.joint);
		Destroy(this.gameObject);
	}
}
