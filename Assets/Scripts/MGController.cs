﻿using UnityEngine;
using System.Collections;

public class MGController : MonoBehaviour {

	private MG left;
	private MG right;
	private LineRenderer a;

	public float damp = 0.8f;
	public float speedFixStep = 0.1f;

	private CharacterUI cUI;

	private Rigidbody rigidbody;
	private ParticleSystem smokePS;

	// Use this for initialization
	void Start () {
		MG[] arr = GetComponentsInChildren<MG>();
		left = arr[0];
		right = arr[1];

		this.rigidbody = this.GetComponent<Rigidbody>();
		this.smokePS = this.GetComponent<ParticleSystem>();

		cUI = this.GetComponent<CharacterUI>();

		a = this.gameObject.AddComponent<LineRenderer>();

		a.material = new Material(Shader.Find("Particles/Additive"));
		a.SetWidth(0.1f, 0.5f);
		a.SetColors(Color.blue, Color.magenta);

		a.SetVertexCount(2);
	}
	
	// Update is called once per frame
	void LateUpdate () {
		//a = lineRenderer
		a.SetPosition(0, this.transform.position);
		float scrollwheel = Input.GetAxis("Mouse ScrollWheel");

		if (Input.GetButtonDown("MGLeft") && cUI.mgHookPos != Vector3.zero){
			left.fire(cUI.mgHookPos);
		}else if (Input.GetButtonUp("MGLeft")){
			left.unHook();
		}

		if (Input.GetButtonDown("MGRight") && cUI.mgHookPos != Vector3.zero){
			right.fire(cUI.mgHookPos);
		}else if (Input.GetButtonUp("MGRight")){
			right.unHook();
		}

		if (scrollwheel< 0){
			if (left.isHooked()){
				left.reel();
			}

			if (right.isHooked()){
				right.reel();
			}
		}else if (scrollwheel > 0){
			if (left.isHooked()){
				left.reelOut();
			}

			if (right.isHooked()){
				right.reelOut();
			}
		}


	}

	public void FixedUpdate(){
		adjustVelocity();
	}

	public void gasBurst(Vector3 direction){
		bool a = left.gasBurst(direction);
		bool b = right.gasBurst(direction);
		if (a || b){
			this.smokePS.Emit(this.transform.position, -direction, 2f, 7f, Color.gray);
			this.smokePS.Play();
		}
	}

	private void adjustVelocity(){
		Vector3 futurePos = this.transform.position + this.rigidbody.velocity * Time.deltaTime;
		if (left.isHooked()) {
			adjustSingle(left, futurePos);
		}

		if (right.isHooked()) {
			adjustSingle(right, futurePos);
		}

	}

	private void adjustSingle(MG mg, Vector3 position){
		//@TODO REDO
		if (!MathUtils.isInsideSphere(position, mg.getRopeLinkPosition(1), mg.getRopeLinkLength(0))){
			//from the player to the point
			Vector3 normal = mg.getRopeSectionDirection(0, 1).normalized; 
			float originalSpeed = this.rigidbody.velocity.magnitude;
			//vector from the player to the hook point whose length is the distance the player is from
			//the shell of the sphere
			Vector3 fromPlayerToShell = normal * (Vector3.Distance(this.transform.position, mg.getRopeLinkPosition(1)) - mg.getRopeLinkLength(0));

			if (Vector3.Angle(this.rigidbody.velocity, normal) >= 90f){
				//--Solution 1
				/*
				Vector3 vel = this.rigidbody.velocity;
				Vector3 projectedVel = Math3D.ProjectPointOnPlane(normal, normal * mg.getRopeLinkLength(0), vel);
				this.rigidbody.velocity = Vector3.Lerp(this.rigidbody.velocity, projectedVel, this.speedFixStep);
				*/

				//--Test solution 2
				Vector3 vel = this.rigidbody.velocity;
				Vector3 projectedVel = Math3D.ProjectVectorOnPlane(normal, vel);
				//this.rigidbody.AddForce(projectedVel, ForceMode.VelocityChange);
				//this.rigidbody.velocity = Vector3.Lerp(this.rigidbody.velocity, projectedVel, this.speedFixStep);
				this.rigidbody.velocity = projectedVel;

				//Vector3 projectedVel = Vector3.Project(this.rigidbody.velocity, normal) * -1f;
				//Vector3 projectedVel = Math3D.ProjectVectorOnPlane(normal * mg.getRopeLinkLength(0), this.transform.position + vel);
				//this.a.SetPosition(1, this.transform.position + projectedVel);//a = lineRenderer
				//this.rigidbody.velocity = Vector3.RotateTowards(vel, projectedVel, Mathf.PI, 1f);
				//this.rigidbody.velocity = projectedVel;

				//this.rigidbody.velocity += projectedVel.normalized;
			}

			if (this.rigidbody.velocity.magnitude > originalSpeed){
				this.rigidbody.velocity = this.rigidbody.velocity.normalized * originalSpeed;
			}

			//this.rigidbody.velocity += fromPlayerToShell * this.damp;
			this.a.SetPosition(1, this.transform.position + normal * mg.getRopeLinkLength(0));//a = lineRenderer
		}
	}

	public bool releaseGas(Vector3 direction){
		direction.y = 0;
		//make sure both release the gas
		bool a = this.left.releaseGas(direction);
		bool b = this.right.releaseGas(direction);
		return a || b;
	}

	public MG getLeft(){
		return this.left;
	}

	public MG getRight(){
		return this.right;
	}
}
