﻿using UnityEngine;
using System.Collections;

public class DebugMenu : UIElement {
	private bool enabled = false;
	private bool showControls = true;
	private bool showStats = true;

	public TestGrabAI aiScript;
	public GameObject player = null;

	private CCharacterController cController = null;
	private MGController mg = null;
	private MG left;
	private MG right;
	private Rigidbody playerBody = null;
	//Debug log
	private static string[] debugLines = new string[10];
	private bool showDebugLines = true;
	private static int lineCount = 0;

	//Strings for labels
	private string statsLabelText;
	private string debugLabelText;
	private string controlsLabelText = "Q - Left hook|E - Right hook\n"+
		"WASD - Movement\n" +
			"Left shift - Gas\n" +
			"Space - Jump\n" +
			"Scroll wheel - Reel\n" +
			"Double tap WASD - Gas burst\n" +
			"7 - Display controls\n" +
			"6 - \"Debug\" menu\n" +
			"P - Lock cursor\n";
	// Use this for initialization
	void Start () {
		if (player != null) {
			cController = player.GetComponent<CCharacterController>();
			mg = player.GetComponent<MGController>();
			MG[] arr = player.GetComponentsInChildren<MG>();

			left = arr[0];
			right = arr[1];
			playerBody = player.GetComponent<Rigidbody>();
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Alpha6)){
			enabled = !enabled;
		}
		if (Input.GetKeyDown(KeyCode.Alpha7)){
			showControls = !showControls;
		}

		if (this.showStats){
			//this.createStatsLabelText();
		}
		if (this.showDebugLines){
			this.createDebugLogText();
		}
	}

	public override void draw(){
		GUI.skin.button.fontSize = 10;
		GUI.skin.box.fontSize = 10;
		GUI.skin.label.fontSize = 10;

		if (this.showStats){
			this.createStatsLabel();
		}

		if (this.showDebugLines){
			this.printDebugLog();
		}
		if (enabled){
			createGUI();
		}
		if(this.showControls){
			this.printControls();
		}
	}

	private void createGUI(){
		GUI.Box(new Rect(5,5,150, 190), "AI test menu");

		if (GUI.Button(new Rect(10, 22, 140, 15), "Try to grab")){
			aiScript.tryToGrab(this.player.transform.position);
		}

		if (GUI.Button (new Rect (10, 37, 140, 15), "Refill gas")) {
			left.refill();
			right.refill();
		}

		if (GUI.Button(new Rect(10,52,140,15), "Auto reel left: " + left.autoReel)){
			left.autoReel = !left.autoReel;
		}

		if (GUI.Button(new Rect(10,67,140,15), "Auto reel right: " + right.autoReel)){
			right.autoReel = !right.autoReel;
		}

		if (GUI.Button(new Rect(10,82,140,15), "Debug log: " + this.showDebugLines)){
			this.showDebugLines = !this.showDebugLines;
		}

		if (GUI.Button(new Rect(10,97,140,15), "Show stats: " + this.showStats)){
			this.showStats = !this.showStats;
		}

		if (GUI.Button(new Rect(10,112,140,15), "Print rope lengths")){
			this.prindRopeLengths();
		}

		left.minimumReRopeDistance = GUI.HorizontalSlider(new Rect(40, 127, 112,15), left.minimumReRopeDistance, 1f, 5f);
		right.minimumReRopeDistance = GUI.HorizontalSlider(new Rect(40, 142, 112,15), right.minimumReRopeDistance, 1f, 5f);

		GUI.Label(new Rect(10,124, 40, 15), "L " + left.minimumReRopeDistance);
		GUI.Label(new Rect(10,139, 40, 15), "R " + right.minimumReRopeDistance);
	}
	private void createStatsLabelText(){
		Vector3 vel = playerBody.velocity;
		
		//Generate the text
		this.statsLabelText = "Gas left 3DMG: " + this.left.gas + "/" + this.left.gasRefillValue + "\n" +
			"Gas Right 3DMG: " + this.right.gas + "/" + this.right.gasRefillValue + "\n" +
			string.Format("Velocity: {0:F2} m/s", vel.magnitude) + "\n" + 
			string.Format("Velocity vector: ({0:F3}, {1:F3}, {2:F3})", vel.x, vel.y, vel.z);
	}
	private void createStatsLabel(){
		float top = 0;
		float left = Screen.width - 250;

		GUI.Label(new Rect (left, top, 300, 100), this.statsLabelText);

	}

	private void createDebugLogText(){
		this.debugLabelText = "";

		int to = DebugMenu.lineCount >= debugLines.Length ? debugLines.Length : lineCount;
		for(int i = 0; i < to; i++){
			this.debugLabelText += "[" + i + "] " + DebugMenu.debugLines[i] + "\n";
		}
	}

	private void printDebugLog(){
		Color old = GUI.contentColor;

		GUI.contentColor = Color.black;
		GUI.Label(new Rect(0,0, 200, 20), "Debug log:");

		GUI.contentColor = Color.red;
		GUI.Label(new Rect(0,14,400,500), this.debugLabelText);

		GUI.contentColor = old;
	}

	private void printControls(){
		Color old = GUI.contentColor;
		GUI.contentColor = Color.black;

		GUI.Label(new Rect(0, Screen.height - 140, 200, 150), this.controlsLabelText);
		GUI.contentColor = old;
	}


	public static void log(string message){
		int shiftTo = DebugMenu.lineCount < (DebugMenu.debugLines.Length - 1) ? DebugMenu.lineCount : (DebugMenu.debugLines.Length - 1);
		//if (DebugMenu.lineCount < (DebugMenu.debugLines.Length - 1)){
		//	DebugMenu.debugLines[DebugMenu.lineCount] = message;
		//	DebugMenu.lineCount++;
		//}else{
		for(int i = shiftTo; i > 0; i--){
			DebugMenu.debugLines[i] = DebugMenu.debugLines[i - 1];
		}
		DebugMenu.debugLines[0] = message;
		DebugMenu.lineCount++;
		//}//
	}

	private void prindRopeLengths(){
		string message = "Left: [";
		for(int i = 0; i < this.left.getLinkCount(); i++){
			message += this.left.getRopeLinkLength(i) + ", ";
		}
		message += "]";
		DebugMenu.log(message);

		message = "Right: [";
		for(int i = 0; i < this.right.getLinkCount(); i++){
			message += this.right.getRopeLinkLength(i) + ", ";
		}
		message += "]";

		DebugMenu.log(message);
	}
}
