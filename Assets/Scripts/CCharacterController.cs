﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CCharacterController : MonoBehaviour {
	public static CCharacterController current;

	private Camera camera;
	private Animator animator;
	//--
	private bool wasOnGroundBefore = true;
	private bool isOnGround = true;

	public float speed = 1f;
	public float jumpSpeed = 5f;

	public float maxSpeed = 100f;
	public float maxLandSpeed = 30f;

	public float health = 100f;
	//--
	public bool ragdoll = false;
	public float ragdollThreshold = 100f;
	public float landRecoverSpeed = 10f;
	public float airRecoverTime = 3f;

	private bool shouldMove = false;
	//--
	private float ragdollTime = 0f;
	private float airTime = 0f;

	private Vector3 facing = Vector3.zero;

	//--
	private MGController mgController;
	//--Double Tap
	private MovementDirection lastDirection;
	private float lastKeyPressTime;
	public float doubleTapTime = 0.2f;

	//
	private Rigidbody rigidbody;

	private enum MovementDirection{
		FORWARD = 0,
		LEFT = 1,
		RIGHT = 2,
		BACKWARD = 3,
		NO_DIRECTION = 4
	}

	// Use this for initialization
	void Start () {
		camera = Camera.main;
		this.rigidbody = this.GetComponent<Rigidbody>();
		animator = this.GetComponent<Animator>();
		mgController = this.GetComponent<MGController>();
		//--Double tap
		lastDirection = MovementDirection.NO_DIRECTION;
		lastKeyPressTime = Time.time;

		current = this;
	}

	public void Update () {
		Vector3 toMove = Vector3.zero;
		//--Check if on ground or in air
		this.updateState();

		this.animator.SetFloat("Speed", this.rigidbody.velocity.magnitude);

		if (canMove()){
			if (Input.GetButton("Forward")){
				toMove += (camera.transform.forward * speed);
				shouldMove = true;
			}else if(Input.GetButton("Backward")){
				toMove += (-camera.transform.forward * speed);
				shouldMove = true;
			}
				
			if(Input.GetButton("Left")){
				toMove += (-camera.transform.right * speed);
				shouldMove = true;
			}else if (Input.GetButton("Right")){
				toMove += (camera.transform.right * speed);
				shouldMove = true;
			}

			toMove.y = 0;
			//Should I add double tab for jumping?
			if (Input.GetButton("Jump")){
				jump();
			}

			//fix the facing if only hooked
			if (this.mgController.getLeft().isHooked() || mgController.getRight().isHooked()){
				this.shouldMove = true;
			}

			this.fixFacing(shouldMove ? toMove : this.facing);

			if (Input.GetKeyDown(KeyCode.Alpha2)){
				DebugMenu.log("Facing: " + facing);
				Debug.DrawRay(this.transform.position, facing, Color.cyan, 5f);
			}
			if (Input.GetKeyDown(KeyCode.Alpha3)){
			    DebugMenu.log("IsOnGround: " + isOnGround);
				DebugMenu.log("Air time: " + airTime);
			}

			if (Input.GetKeyDown(KeyCode.Alpha4)){
				RaycastHit[] hits = Physics.RaycastAll(new Ray(this.transform.position, Camera.main.transform.forward), 100f);
				if (hits.Length != 0){
					for(int i = 0; i < hits.Length; i++){
						DebugMenu.log("hits[" + i + "]: " + hits[i].transform);
					}
				}else{
					DebugMenu.log("Raycast didn't hit anything.");
				}
			}
			face(facing);

			//this.transform.position += toMove * Time.deltaTime;
			move(toMove);

			if (Input.GetButtonDown("Forward") || Input.GetButtonDown("Backward")|| Input.GetButtonDown("Left") || Input.GetButtonDown("Right")){
				if (this.checkForDoubleTap()){
					mgController.gasBurst(toMove);
				}
			}

		}else{
			ragdollTime += Time.deltaTime;
			if(canRecover()){
				recover();
			}
		}

		//Fix the speed
		if (rigidbody.velocity.magnitude > maxSpeed){
			rigidbody.velocity = rigidbody.velocity.normalized * maxSpeed;
		}

		this.handleParticles();

		//The fix made to the velocity made based on the rope link positions is made on the MGController
	}
	//--End of update

	public void move(Vector3 velocity){
		if (isOnGround){
			if (this.rigidbody.velocity.magnitude > this.maxLandSpeed){
				//sliding
				return;
			}

			this.GetComponent<Rigidbody>().AddForce(velocity, ForceMode.VelocityChange);

			if (this.rigidbody.velocity.magnitude > this.maxLandSpeed){
				this.rigidbody.velocity = this.rigidbody.velocity.normalized * this.maxLandSpeed;
			}
		}else{

		}

		if (Input.GetKey(KeyCode.LeftShift)){
			mgController.releaseGas(velocity);
		}
	}

	public bool canMove(){
		return !ragdoll;
	}

	private void face(Vector3 direction){
		this.transform.LookAt(new Vector3(this.transform.position.x + direction.x,
		                                  //this.transform.position.y,
		                                  this.transform.position.y,
		                                  this.transform.position.z + direction.z));
	}
	//facing = toMove;
	//fix the facing for the player, if the player has only 1 hook, face that, 
	//if it has two, face the middle, if has none, face the pressed movement keys.
	//Maaaaaybe mix the hooked points with the movement direction
	private void fixFacing(Vector3 toMove){
		Vector3 tmpFacing = this.facing;
		bool isLeftHooked = mgController.getLeft().isHooked();
		bool isRightHooked = mgController.getRight().isHooked();

		if (isLeftHooked && isRightHooked){
			//get the two vectors and face the middle
			Vector3 left = mgController.getLeft().getRopeSectionDirection(0, 1);
			Vector3 right = mgController.getRight().getRopeSectionDirection(0, 1);
			tmpFacing = (left + right) / 2;
		}else if (isLeftHooked){
			tmpFacing = mgController.getLeft().getRopeSectionDirection(0, 1);
		}else if (isRightHooked){
			tmpFacing = mgController.getRight().getRopeSectionDirection(0, 1);
		}else if (toMove != Vector3.zero){
			tmpFacing = toMove;
			//tmpFacing.y = 1;
		}
		
		this.facing = new Vector3(Mathf.Lerp(facing.x, tmpFacing.x, 0.2f),
		                          Mathf.Lerp(facing.y, tmpFacing.y, 0.2f),
		                          Mathf.Lerp(facing.z, tmpFacing.z, 0.2f));
	}

	public void OnCollisionEnter(Collision c){
		foreach (ContactPoint contact in c.contacts) {
			Debug.DrawRay(contact.point, contact.normal, Color.white, 5f);
		}

		if (c.relativeVelocity.magnitude > ragdollThreshold){
			this.ragdoll = true;
			DebugMenu.log("Health damage: " + this.getHealthDamage(c.relativeVelocity.magnitude));
		}
	}

	public void OnCollisionExit(Collision c){
		//@TODO?
	}

	public bool canRecover(){
		//is on ground and the speed is lower than the recover speed
		//or 
		//is on the air for long enough
		return (isOnGround && rigidbody.velocity.magnitude <= landRecoverSpeed) ||
			(airTime >= airRecoverTime);
	}

	public void recover(){
		//for the player to recover he must be a ragdoll
		if (ragdoll){
			this.ragdoll = false;

			if (isOnGround){
				this.airTime = 0f;
			}else{
				//@TODO
			}
		}
	}

	private float getHealthDamage(float speed){
		return Mathf.Pow(Mathf.Log(speed), 3);
	}

	public void setFacingBasedOnCamera(){
		this.face (Camera.main.transform.forward);
	}

	private void updateState(){
		Vector3 raycastPos = this.transform.position;
		raycastPos.y += 0.5f;
		bool isOnGroundNow = Physics.Raycast(new Ray(raycastPos, Vector3.down), 0.55f);
		Debug.DrawRay(raycastPos, Vector3.down * 0.55f, Color.white, 5f);

		if (isOnGroundNow){
			airTime = 0f;
			//if the player's speed is below 3f it keeps bouncing, so lerp that to 0
			if (this.rigidbody.velocity.magnitude < 4.5f){
				this.rigidbody.velocity = Vector3.Lerp(this.rigidbody.velocity, Vector3.zero, 0.1f);
			}
		}else{
			airTime += this.ragdoll ? Time.deltaTime : 0f;

			if (this.rigidbody.velocity.magnitude > maxSpeed){
				this.rigidbody.velocity = this.rigidbody.velocity.normalized * maxSpeed;
			}
		}

		this.animator.SetBool("Jump", false);
		this.wasOnGroundBefore = this.isOnGround;
		this.isOnGround = isOnGroundNow;
	}

	public void jump(){
		if (isOnGround){
			Vector3 jumpSpeedDirection = this.camera.transform.forward;
			jumpSpeedDirection.y = jumpSpeed;
			//this.rigidbody.velocity += jumpSpeedDirection;
			this.rigidbody.AddForce(jumpSpeedDirection, ForceMode.VelocityChange);
			animator.SetBool("Jump", true);
		}
	}
	//This does not have a failsafe aginst 3 key presses
	//I might add that, but I think its better not to yet
	private bool checkForDoubleTap(){
		MovementDirection dir = this.lastDirection;

		if (Input.GetButtonDown("Forward")){
			dir = MovementDirection.FORWARD;
		}else if (Input.GetButtonDown("Backward")){
			dir = MovementDirection.BACKWARD;
		}else if (Input.GetButtonDown("Left")){
			dir = MovementDirection.LEFT;
		}else if (Input.GetButtonDown("Right")){
			dir = MovementDirection.RIGHT;
		}

		if (this.lastDirection == dir && (Time.time - this.lastKeyPressTime) < this.doubleTapTime){
			this.lastKeyPressTime = Time.time;
			this.lastDirection = dir;
			return true;
		}else{
			this.lastKeyPressTime = Time.time;
			this.lastDirection = dir;
			return false;
		}
		
	}

	private void handleParticles(){
		if (Input.GetKey(KeyCode.LeftShift)){
			if (this.mgController.getLeft().gas > 0 && this.mgController.getRight().gas > 0){
				this.GetComponent<ParticleSystem>().enableEmission = true;
			}
		}else{
			this.GetComponent<ParticleSystem>().enableEmission = false;
		}
	}
}
