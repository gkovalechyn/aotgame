﻿using UnityEngine;
using System.Collections;
//The 3D maneuver gear (3DMG)

/*This script represents one side of the 3DMG, so for the full effectiveness
 * of the 3DMG, there should be two objects, one in each side of the game
 * object with this script.
 * 
 */
public class MG : MonoBehaviour {
	public float reelForce = 10f;
	public float ropeLength = 90f;//90 meters
	public float reelLength = 1f;

	public float constantPull = 10F;

	//--Gas stuff
	public float gasDecreasePerReel = 0.25f;
	public float gasDecreasePerFire = 0.75f;
	public float gasDecreasePerUse = 0.02f;

	public float gas = 100f;
	public float gasForce = 2f;
	public float gasRefillValue = 100f;

	public float burstGasDecrease = 2f;
	public float gasBurstForce = 20f;
	public bool dynamicGasBurstForce = false;
	//--
	public float minimumReRopeDistance = 5f;
	public float minimumUnropeAngle = 10f;
	//--
	public bool autoReel = true;
	//--
	public LayerMask ropePassThroughLayers;
	//--
	private bool hooked = false;
	public GameObject hookPrefab;
	public Hook hookP = null;//Hook point
	public float hookFireVelocity = 100f;
	//--
	private LineRenderer renderer;

	//The first point will always be the position of the hook launcher
	private ArrayList ropePoints = new ArrayList();

	private Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
		renderer = this.gameObject.AddComponent<LineRenderer>();

		renderer.material = new Material(Shader.Find("Particles/Additive"));
		renderer.SetWidth(0.3f, 1f);
		renderer.SetColors(Color.green, Color.red);

		this.rigidbody = this.transform.root.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update() {
		//checkForFreeRope();
		//Update the first point of the rope

		//this.fixPlayerRopePosition(this.fixRopePointLegths());

		render();
	}

	void FixedUpdate(){
		//recalculate the distance independent of autoReeling is enabled and if it only has more than two rope links,
		//Because the original link might be hooked into something that moves.
		checkForCollisions();
		
		if (autoReel){
			doAutoReeling();
		}

		/*
		if (this.getLinkCount() >= 2){
			//Add the constant force.
			this.rigidbody.AddForce(this.getRopeSectionDirection(0, 1).normalized * this.constantPull);

		}
		*/
		this.updateRopeLinkPositions();
	}

	//--
	public void hook(RaycastHit point){
		RopeLinkData data = new RopeLinkData();
		data.pos = point.point;
		data.linkedTo = point.transform;
		data.linkNormal = point.normal;
		data.offset = point.point - data.linkedTo.transform.position;

		RopeLinkData first = new RopeLinkData();
		first.linkedTo = this.transform;
		first.pos = this.transform.position;
		first.linkNormal = (data.pos - this.transform.position).normalized;
		first.originalRopeLength = Vector3.Distance(point.point, this.transform.position);

		ropePoints.Clear();
		ropePoints.Add(first);
		ropePoints.Add(data);
		this.hooked = true;
	}
	//0 = closest link
	//1 - n-2 = middle links
	//last = original hook hit point
	private float updateRopeLinkPositions(){
		float distanceChanged = 0f;

		if (ropePoints.Count >= 2){
			//the hook point is a special case
			RopeLinkData hook = (RopeLinkData) ropePoints[ropePoints.Count - 1];
			hook.pos = this.hookP.joint.transform.position;

			//the first position will be changed by auto-reeling but it's position must be set either way
			for(int i = ropePoints.Count - 2; i >= 0; i--){
				RopeLinkData data = (RopeLinkData) ropePoints[i];
				data.pos = data.linkedTo.transform.position + data.offset;

				if (i != 0){
					float distance = Vector3.Distance(data.pos, ((RopeLinkData)ropePoints[i + 1]).pos);
					distanceChanged += distance - data.originalRopeLength;
					data.originalRopeLength = distance;
				}
			}
		}

		return distanceChanged;
	}

	public void unHook(){
		DebugMenu.log("Rope broke.");
		this.hooked = false;
		//clear the hook
		this.hookOutOfRange();
		//The hook is already destoyed in hookOutOfRange()
		//this.hookP.unhook();
		this.ropePoints.Clear();
	}

	public bool releaseGas(Vector3 direction){
		if (this.gas > 0){
			gas -= gasDecreasePerUse;
			//this.transform.parent.rigidbody.velocity += direction * gasForce;
			//UNITY5: this.transform.parent.GetComponent<Rigidbody>().AddForce(direction * gasForce, ForceMode.VelocityChange);
			this.rigidbody.AddForce(direction * gasForce, ForceMode.VelocityChange);
			return true;
		}
		return false;
	}

	public bool gasBurst(Vector3 direction){
		if(dynamicGasBurstForce){
			if (gas > burstGasDecrease){
				gas -= burstGasDecrease;
				
				//this.transform.parent.rigidbody.velocity += direction.normalized * gasBurstForce;
				//UNITY5: this.transform.parent.GetComponent<Rigidbody>().AddForce(direction.normalized * gasBurstForce, ForceMode.VelocityChange);
				this.rigidbody.AddForce(direction.normalized * gasBurstForce, ForceMode.VelocityChange);
			}else{
				if (gas <= 0){
					return false;
				}

				//% of gas that you have
				float multiplier = burstGasDecrease / gas;

				//this.transform.parent.rigidbody.velocity += direction.normalized * gasBurstForce * multiplier;
				//UNITY5: this.transform.parent.GetComponent<Rigidbody>().AddForce(direction.normalized * gasBurstForce * multiplier, ForceMode.VelocityChange);
				this.rigidbody.AddForce(direction.normalized * gasBurstForce * multiplier, ForceMode.VelocityChange);

				gas = 0;
			}
			return true;
		}else{
			if(gas - burstGasDecrease > 0){
				gas -= burstGasDecrease;
				
				//this.transform.parent.rigidbody.velocity += direction.normalized * gasBurstForce;
				//UNITY5: this.transform.parent.GetComponent<Rigidbody>().AddForce(direction.normalized * gasBurstForce, ForceMode.VelocityChange);
				this.rigidbody.AddForce(direction.normalized * gasBurstForce, ForceMode.VelocityChange);
				return true;
			}
			return false;
		}
	}

	public void reel(){
		if (ropePoints.Count >= 2 && gas > 0){
			//Combine adding force and making the length smaller
			Vector3 forceDirection = this.getRopeSectionDirection(0, 1);
			Vector3 force = forceDirection.normalized * this.reelForce;

			//this.transform.parent.rigidbody.velocity += force;
			//UNITY5: this.transform.parent.GetComponent<Rigidbody>().AddForce(force, ForceMode.VelocityChange);
			this.rigidbody.AddForce(force, ForceMode.VelocityChange);

			this.gas -= gasDecreasePerReel;

			if (this.getRopeLinkLength(0) > reelLength){
				RopeLinkData data = (RopeLinkData) ropePoints[0];
				data.originalRopeLength -= reelLength;
				ropePoints[0] = data;
			}
		}
	}

	public void reelOut(){
		if (ropePoints.Count >= 2 && gas > 0){
			RopeLinkData data = (RopeLinkData) ropePoints[0];
			data.originalRopeLength += reelLength;
		}
	}

	public void fire (Vector3 target){
		//hookP == null -> Hasn't fired.
		if (gas - gasDecreasePerFire > 0 && this.hookP == null) {
			GameObject hook = (GameObject) Instantiate(this.hookPrefab);
			DontGoThroughThings dgttScript = hook.GetComponent<DontGoThroughThings>();
			this.hookP = hook.GetComponent<Hook>();


			this.hookP.setLauncher(this);
			//this.hookP.transform.position += new Vector3(0f, 1f, 0f);
			this.hookP.transform.position = this.transform.position + this.transform.forward;
			this.hookP.transform.parent = null;
			this.hookP.fired = true;

			dgttScript.initValues();

			//Ray r = new Ray (this.transform.position, Camera.main.transform.forward);
			//RaycastHit hit;

			gas -= gasDecreasePerFire;
			//changed so that the hook reaches the passed target position and isn't
			//affected by gravity
			//hookP.rigidbody.velocity = Camera.main.transform.forward * this.hookFireVelocity;
			hookP.GetComponent<Rigidbody>().useGravity = false;
			hookP.GetComponent<Rigidbody>().velocity = (target - hookP.transform.position).normalized * this.hookFireVelocity;
		}
	}

	private void render(){
		if (ropePoints.Count >= 2){
			renderer.enabled = true;

			renderer.SetVertexCount(ropePoints.Count);

			for (int i = 0; i < this.ropePoints.Count; i++){
				renderer.SetPosition(i, ((RopeLinkData) ropePoints[i]).pos);
			}
		}else{
			renderer.enabled = false;
		}
	}

	void checkForCollisions(){
		if (ropePoints.Count >= 2){
			float totalDistance = 0f;

			for (int i = 0; i < ropePoints.Count - 1; i++){
				Ray r = new Ray(((RopeLinkData)ropePoints[i]).pos, ((RopeLinkData)ropePoints[i + 1]).pos - ((RopeLinkData)ropePoints[i]).pos);
				RaycastHit hit;
				float distance = Vector3.Distance(((RopeLinkData)ropePoints[i + 1]).pos, ((RopeLinkData)ropePoints[i]).pos) * 0.93f;

				totalDistance += distance / 0.93f;

				if (totalDistance > ropeLength){
					breakRope();
				}else{
					//@TODO swap with RaycastAll
					if (Physics.Raycast(r, out hit, distance)){
						//if (hit.transform.tag != "Player"){
						if (Vector3.Distance(hit.point, ((RopeLinkData) ropePoints[i]).pos) >= minimumReRopeDistance &&
				    		Vector3.Distance(hit.point, ((RopeLinkData) ropePoints[i + 1]).pos)>= minimumReRopeDistance){
							//Add a point to the rope
							RopeLinkData data = new RopeLinkData();

							data.pos = hit.point;
							data.linkNormal = hit.normal;
							data.offset = hit.point - hit.transform.position;
							data.linkedTo = hit.transform;
							ropePoints.Insert(i + 1, data);

							DebugMenu.log("Hit position: " + hit.point);
							DebugMenu.log("Transform position: " + hit.transform.position);
							DebugMenu.log("data.pos: " + data.pos);
							DebugMenu.log("data.offset: " + data.offset);

							//already done after this method
							this.updateRopeLinkPositions();
							//fixRopePointLegths();
							//Avoid miscalculations
							return;
						}
						//}
					}//raycast

				}//totalDistance > ropeLength
			}//for RopeLinkData
		}
	}

	void checkForFreeRope(){
		if (ropePoints.Count >= 3){
			//vec1(orig), vec2(intercept0),..., vec3(hookPoint)
			//Wee need to check only the first three points, because the other ones will not be unhooked
			//unless these are
			Vector3 firstRopeSection = this.getRopeSectionDirection(0,1);
			Vector3 secondRopeSection = this.getRopeSectionDirection(1,2);
			float angle = Vector3.Angle(firstRopeSection, secondRopeSection);
		}
	}
	void breakRope(){
		unHook();
	}

	public bool isHooked(){
		return hooked && ropePoints.Count >= 2;
	}
	/**
	 * Returns a Vector pointing from pos1 to pos2
	 */
	public Vector3 getRopeSectionDirection(int pos1, int pos2){
		if (pos1 >= ropePoints.Count || pos1 < 0){
			throw new System.Exception("Invalid value: " + pos1 + "{Count = " + ropePoints.Count + "}");
		}

		if (pos2 >= ropePoints.Count || pos2 < 0){
			throw new System.Exception("Invalid value: " + pos2 + "{Count = " + ropePoints.Count + "}");
		}

		return (((RopeLinkData) ropePoints[pos2]).pos - ((RopeLinkData) ropePoints[pos1]).pos);
	}

	public Vector3 getRopeLinkPosition(int pos){
		if (pos >= ropePoints.Count || pos < 0){
			throw new System.Exception("Invalid value: " + pos + "{Count = " + ropePoints.Count + "}");
		}

		return ((RopeLinkData) ropePoints[pos]).pos;
	}
	/**Fixed the rope lengths, only if the new distance is smaller than the old one, if the new distance is
	 * longer than the old one,
	 * 
	 * @return The distance it was enlarged, if it was positive something pulled the ropePoints if it is negative
	 * something made the ropePoints closer so the adjusting is only needed if this is positive.
	 */
	private float fixRopePointLegths(){
		float lenghEnlarged = 0f;

		//only adjust after the first point
		for(int i = 1; i < ropePoints.Count - 1; i++){
			RopeLinkData first = (RopeLinkData) ropePoints[i];
			RopeLinkData second = (RopeLinkData) ropePoints[i + 1];
			float newDistance = Vector3.Distance(first.pos, second.pos);

			lenghEnlarged += newDistance - first.originalRopeLength;

			first.originalRopeLength = newDistance;
			second.originalRopeLength = 0f;
		}

		return lenghEnlarged;
	}

	public int getLinkCount(){
		return this.ropePoints.Count;
	}

	public float getRopeLinkLength(int pos){
		if (pos >= ropePoints.Count || pos < 0){
			throw new UnityException("Invalid value: " + pos + "{Count = " + ropePoints.Count + "}");
		}
		return ((RopeLinkData) ropePoints[pos]).originalRopeLength;
	}

	private void doAutoReeling(){
		if (ropePoints.Count >= 2){
			RopeLinkData first = (RopeLinkData) ropePoints[0];
			RopeLinkData second = (RopeLinkData) ropePoints[1];

			float distance = Vector3.Distance(first.pos, second.pos);

			if (distance < first.originalRopeLength){
				first.originalRopeLength = distance;
			}
		}
	}

	public void hookHit(){
		RopeLinkData data = new RopeLinkData();
		data.pos = this.hookP.joint.transform.position;
		data.linkedTo = this.hookP.transform;
		//data.offset = this.hookP.transform.position - this.hookP.hit.transform.position;
		data.linkNormal = this.hookP.normal;
		
		RopeLinkData first = new RopeLinkData();
		first.linkedTo = this.transform;
		first.pos = this.transform.position;
		first.linkNormal = (data.pos - this.transform.position).normalized;
		first.originalRopeLength = Vector3.Distance(data.pos, this.transform.position);
		
		ropePoints.Clear();
		ropePoints.Add(first);
		ropePoints.Add(data);
		this.hooked = true;
	}

	public void hookOutOfRange(){
		//Delete the hook.
		if (this.hookP != null){
			Destroy(hookP.gameObject);
			this.hookP = null;
		}
	}

	public void refill(){
		this.gas = this.gasRefillValue;
	}

	private sealed class RopeLinkData{
		public Transform linkedTo;
		public Vector3 pos;
		public Vector3 offset;
		public Vector3 linkNormal;
		public float originalRopeLength = 0f;
	}
}


