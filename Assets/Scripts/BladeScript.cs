﻿using UnityEngine;
using System.Collections;

public class BladeScript : MonoBehaviour {
	private float durability = 100f;
	public const float MAX_DURABILITY = 100f;

	private const float DAMAGE_MULTIPLIER = 7.6f;

	private bool isAttacking = true;

	private float attackTime;

	// Use this for initialization
	void Start () {
		attackTime = Time.time;
	}
	
	// Update is called once per frame
	void FixedUpdate () {

	}

	public void OnTriggerEnter(Collider col){
		IDamageable damageable = col.gameObject.GetComponent(typeof(IDamageable)) as IDamageable;

		if (damageable != null){
			//Blade -> 3DMG(MG script) -> Player (Root component)
			Vector3 parentVel = this.transform.root.GetComponent<Rigidbody>().velocity;
			Vector3 objectVel;
			Vector3 relativeVelocity;

			if (col.gameObject.GetComponent<Rigidbody>() == null){
				objectVel = Vector3.zero;
			}else{
				objectVel = col.gameObject.GetComponent<Rigidbody>().velocity;
			}

			relativeVelocity = parentVel - objectVel;

			damageable.damage(this.getDamage(relativeVelocity.magnitude));

			DebugMenu.log("Hit relative velocity: " + relativeVelocity.magnitude);
			DebugMenu.log("Hit damage: " + this.getDamage(relativeVelocity.magnitude));
			DebugMenu.log("Blade damage: " + this.getBladeDamage(relativeVelocity.magnitude));
		}
	}

	/**
	 * Damage done TO the blade
	 * 
	 */
	private float getBladeDamage(float relativeVel){
		//low speed = high damage
		//high speed = low damage
		//return (-relativeVel/80f + 20f);
		if (this.isAttacking){
			if (relativeVel < 1f){
				return 100f;
			}

			float maxDamage = 500f/relativeVel;
			float mult = Mathf.Min(1, Time.time - this.attackTime);

			return maxDamage * mult;
		}else{
			return 0;
		}
	}
	/**
	 * Damage dealt by the blade
	 *
	*/
	public float getDamage(float velocity){
		if (this.isAttacking){
			return velocity * BladeScript.DAMAGE_MULTIPLIER * this.getBladePercentage();
		}else{
			return 0;
		}
	}

	public float getBladePercentage(){
		return BladeScript.MAX_DURABILITY / this.durability;
	}
	public float getHealth(){
		return this.durability;
	}

	public void setAttacking(bool attacking){
		this.isAttacking = attacking;

		if (attacking){
			this.attackTime = Time.time;
		}
	}	
}
