﻿using UnityEngine;
using System.Collections;

public class MovingObject : MonoBehaviour {
	public Vector3 from;
	public Vector3 to;
	public Vector3 offset;

	public bool offsetMode = false;

	public float tolerance = 1f;
	private float toleranceSquared;
	public float step = 0.1f;

	// Use this for initialization
	void Start () {
		toleranceSquared = tolerance * tolerance;

		if (from == null){
			from = this.transform.position;
		}

		if (offsetMode){
			this.to = this.from + offset;
		}else if (this.to == null){
			Destroy(this);
		}

	}

	void FixedUpdate () {
		Vector3 offset = to - this.transform.position;

		if (offset.sqrMagnitude < toleranceSquared){
			Vector3 tmp = from;
			from = to;
			to = tmp;
		}

		this.transform.position = this.transform.position + offset.normalized * step;

	}
}
