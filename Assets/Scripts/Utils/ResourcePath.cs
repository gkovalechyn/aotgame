﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class ResourcePath {
    public static ResourcePath TITAN = new ResourcePath("Titan");
    public static ResourcePath PLAYER = new ResourcePath("unitychan3");


    private readonly string path;
    private ResourcePath(string path) {
        this.path = path;
    }

    public string getPath() {
        return this.path;
    }
}
