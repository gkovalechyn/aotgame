//------------------------------------------------------------------------------
// <auto-generated>
//     O código foi gerado por uma ferramenta.
//     Versão de Tempo de Execução:4.0.30319.34014
//
//     As alterações ao arquivo poderão causar comportamento incorreto e serão perdidas se
//     o código for gerado novamente.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using UnityEngine;

public class MathUtils{

	public static bool isInsideAABB(Vector3 point, Vector3 min, Vector3 max){
		//no need to create new Vectors
		float minX = Math.Min(min.x, max.x);
		float minY = Math.Min(min.y, max.y);
		float minZ = Math.Min(min.z, max.z);

		float maxX = Math.Min(min.x, max.x);
		float maxY = Math.Min(min.y, max.y);
		float maxZ = Math.Min(min.z, max.z);

		return (point.x >= minX && point.x <= maxX) && (point.y >= minY && point.y <= maxY) && (point.z >= minZ && point.z <= maxZ);
	}

	public static bool isInsideSphere(Vector3 point, Vector3 center, float radius){
		return ((point.x - center.x) * (point.x - center.x) + 
		        (point.y - center.y) * (point.y - center.y) + 
		        (point.z - center.z) * (point.z - center.z)) <= radius * radius;
	}

	public static void wrapAngles(ref float angle){
		if(angle < -360F){
			angle += 360F;
		}

		if(angle > 360F){
			angle -= 360F;
		}
	}
	/// <summary>
	/// Rotate a transform.
	/// </summary>
	/// <param name="transform">The transform to be rotated.</param>
	/// <param name="x">The amount rotate left and right.</param>
	/// <param name="y">The amount to tilt up and down.</param>
	public static void rotate(Transform transform, float x, float y){
		//Vector3 localAngles = transform.localEulerAngles;
		//transform.RotateAround(transform.position, -transform.right, y);
		//transform.RotateAround(transform.position, transform.up, x);
		//Quaternion rotationX = Quaternion.AngleAxis(x, transform.up);
		//Quaternion rotationY = Quaternion.AngleAxis(y, -transform.right);
		//transform.rotation = transform.rotation * rotationX * rotationY * Quaternion.AngleAxis(Quaternion.Dot(rotationX, rotationY), transform.forward);
		//transform.rotation = transform.rotation * Quaternion.AngleAxis(y, -transform.right);// * Quaternion.AngleAxis(x, transform.up);
		//transform.rotation = transform.rotation * Quaternion.AngleAxis(x, transform.up);
		transform.rotation = transform.rotation * Quaternion.AngleAxis(y, -transform.right) * Quaternion.AngleAxis(x, transform.up);
		//localAngles.y += x;
		//localAngles.x -= y;

		//MathUtils.wrapAngles(ref localAngles.x);
		//MathUtils.wrapAngles(ref localAngles.y);

		//transform.localEulerAngles = localAngles;
	}
	/// <summary>
	/// Checks if a Vector is to the left or right of another vector.
	/// http://forum.unity3d.com/threads/left-right-test-function.31420/
	/// </summary>
	/// <returns>Returns a negative number if B is left of A, positive if right of A, or 0 if they are perfectly aligned.</returns>
	/// <param name="A">A - Main vector</param>
	/// <param name="B">B - Vector to be checked</param>
	public static float AngleDir(Vector2 A, Vector2 B){
		return -A.x * B.y + A.y * B.x;
	}

	/// <summary>
	/// Checks if a Vector is to the left or right of another vector.
	/// (Uses X and Z)
	/// http://forum.unity3d.com/threads/left-right-test-function.31420/
	/// </summary>
	/// <returns>Returns a negative number if B is left of A, positive if right of A, or 0 if they are perfectly aligned.</returns>
	/// <param name="A">A - Main vector</param>
	/// <param name="B">B - Vector to be checked</param>
	public static float AngleDir(Vector3 A, Vector3 B){
		return -A.x * B.z + A.z * B.x;
	}
}

