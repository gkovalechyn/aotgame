using UnityEngine;
using System.Collections;

public class Utils{

	public static PrimitiveType primitiveFromInt(int id){
		switch(id){
			case 0:
				return PrimitiveType.Sphere;//Sphere
			case 1:
				return PrimitiveType.Capsule;//capsule
			case 2: 
				return PrimitiveType.Cylinder;
			case 3:
				return PrimitiveType.Cube; //Cube
			case 4:
				return PrimitiveType.Plane; //Plane
			case 5:
				return PrimitiveType.Quad;//Quad
			default:
				return PrimitiveType.Sphere;
		}
	}

	public static LightType lightFromInt(int id){
		switch (id){
			case 0:
				return LightType.Spot;
			case 1:
				return LightType.Directional;
			case 2:
				return LightType.Point;
			case 3:
				return LightType.Area;
			default:
				return LightType.Directional;
		}
	}
}

