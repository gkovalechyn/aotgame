using UnityEngine;
using System.Collections;
using System.IO;
using SimpleJSON;

public class Options{
	public const string FILE_PATH = "./Options.json";
	public static float fov = 90F;
	//Graphics
	public static float aspectRatio = 16F/9F;
	//0=Disable;1=Enable;2=ForceEnable
	public static int anisotropicFiltering = 1;
	//0;2;4;8
	public static int antiAliasing = 0;
	//0 = Unity; 1 = FXAA3
	public static int antiAliasingType = 0;

	public static float shadowDistance = 100F;
	public static bool softVegetation = false;
	public static bool realtimeReflectionProbes = false;
	public static float drawDistance = 2000F;

	//Sound (0.0 to 1.0)
	public static float musicVolume = 1F;
	public static float soundVolume = 1F;

	public static float mouseSensitivityX = 1f;
	public static float mouseSensitivityY = 1f;

	public static int vSyncLevel = 0;
	public static int framerateCap = 120;

	public static void save(){
		JSONClass obj = new JSONClass();

		obj["Field of View"].AsFloat = fov;
		obj["Music volume"].AsFloat = musicVolume;
		obj["Sound volume"].AsFloat = soundVolume;
		obj["Aspect ratio"].AsFloat = aspectRatio;
		obj["Sensitivity X"].AsFloat = mouseSensitivityX;
		obj["Sensitivity Y"].AsFloat = mouseSensitivityY;
		obj["VSync level"].AsInt = vSyncLevel;
		obj["Framerate cap"].AsInt = framerateCap;
		obj["Anti Aliasing"].AsInt = antiAliasing;
		obj["Anti aliasing type"].AsInt = antiAliasingType;
		obj["Shadow distance"].AsFloat = shadowDistance;
		obj["Soft vegetation"].AsBool = softVegetation;
		obj["Realtime reflection probes"].AsBool = realtimeReflectionProbes;
		obj["Draw distance"].AsFloat = drawDistance;

        if(File.Exists(Options.FILE_PATH)) {
            File.Delete(Options.FILE_PATH);
        }

		obj.SaveToFile(Options.FILE_PATH);
	}

	public static void load(){
		if (!File.Exists("./" + Options.FILE_PATH)){
			Options.save();
		}
		JSONClass obj = (JSONClass) JSONClass.LoadFromFile(Options.FILE_PATH);

		fov = obj["Field of View"].AsFloat;
		musicVolume = obj["Music volume"].AsFloat;
		soundVolume = obj["Sound volume"].AsFloat;
		aspectRatio = obj["Aspect ratio"].AsFloat;
		mouseSensitivityX = obj["Sensitivity X"].AsFloat;
		mouseSensitivityY = obj["Sensitivity Y"].AsFloat;
		vSyncLevel = obj["VSync level"].AsInt;
		framerateCap = obj["Framerate cap"].AsInt;
		antiAliasing = obj["Anti Aliasing"].AsInt;
		antiAliasingType = obj["Anti aliasing type"].AsInt;
		shadowDistance = obj["Shadow distance"].AsFloat;
		softVegetation = obj["Soft vegetation"].AsBool;
		realtimeReflectionProbes = obj["Realtime reflection probes"].AsBool;
		drawDistance = obj["Draw distance"].AsFloat;
	}
}


