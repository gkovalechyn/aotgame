﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class CustomDataName {
    public static CustomDataName CUSTOM_LEVEL_LOAD = new CustomDataName("CustomLevelToLoad");

    private readonly string path;
    private CustomDataName(string path) {
        this.path = path;
    }

    public override string ToString() {
        return path;
    }
} 
