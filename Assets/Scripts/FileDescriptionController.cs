﻿using UnityEngine;
using System.Collections;

public class FileDescriptionController : MonoBehaviour {
	private FileChooserController parent;
	private int id;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void setParentController(FileChooserController parent){
		this.parent = parent;
	}

	public void onClick(){
		this.parent.itemRecievedClick(this.id);
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return this.id;
	}
}