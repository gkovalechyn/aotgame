﻿using UnityEngine;
using System.Collections;

public class TitanHitboxScript : MonoBehaviour, IDamageable {
	private float nextHit;
	private float hitInterval = 1f;

	// Use this for initialization
	void Start () {
		nextHit = Time.time + hitInterval;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public bool onHit(BladeScript blade){
		if (Time.time - nextHit >= 0){
			nextHit = Time.time + hitInterval;
			return true;
			//deal damage
		}
		return false;
	}

	public void damage (float damage) {
		throw new System.NotImplementedException ();
	}
}
