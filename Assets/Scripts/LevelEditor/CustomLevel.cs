using UnityEngine;
using System.Collections;
using SimpleJSON;
public class CustomLevel{
	private JSONClass level;
	private JSONClass navmesh;
	private JSONClass textures;
    private JSONClass map;
	private LevelItem[] items;

	public void setNavmesh(JSONClass navmesh){
		this.navmesh = navmesh;
	}

	public void setTextures(JSONClass textures){
		this.textures = textures;
	}

    public void setLevelInfo(JSONClass clazz) {
        this.level = clazz;
    }
	public JSONClass getLevelInfo(){
		return this.level;
	}

	public JSONClass getNavmesh(){
		return this.navmesh;
	}

	public JSONClass getTextures(){
		return this.textures;
	}

	public LevelItem[] getItems(){
		return this.items;
	}

    public JSONClass getMap() {
        return this.map;
    }

	public void setItems(LevelItem[] items){
		this.items = items;
        this.map = new JSONClass();

        foreach(LevelItem item in items) {
            JSONClass clazz = new JSONClass();
            item.save(clazz);
            this.map["Items"][-1] = clazz;
        }
	}
}

