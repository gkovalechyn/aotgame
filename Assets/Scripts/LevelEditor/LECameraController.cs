﻿using UnityEngine;
using System.Collections;

public class LECameraController : MonoBehaviour {
	private float rotationX = 0;
	private float rotationY = 0;

	public bool toMove = true;

	public float moveAmount = 10F;
	public float damp = 0.2F;

	public CursorMode cursorMode = CursorMode.Auto;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (this.toMove){
			if (Input.GetMouseButton(1)){
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;

				Vector3 toMove = Vector3.zero;
				float amount = this.moveAmount;
				float dX = Input.GetAxis("Mouse X") * Options.mouseSensitivityX;
				float dY = Input.GetAxis("Mouse Y") * Options.mouseSensitivityY;
				
				rotationX += dX;
				rotationY += dY;
				
				if (rotationX > 360){
					rotationX -= 360;
				}else if (rotationX < -360){
					rotationX += 360;
				}
				rotationY = Mathf.Clamp(rotationY, -80F, 80F);
				
				this.transform.rotation = Quaternion.Euler(-rotationY, rotationX, 0F);
				//this.rotate(-dY, dX);
				//MathUtils.rotate(this.transform, dX, dY);-
				//this.transform.Rotate(-Vector3.right, dY, Space.Self);
				//this.transform.Rotate(Vector3.up, dX, Space.Self);
				//Quaternion rot = this.transform.rotation;
				//rot.z = 0;
				//this.transform.rotation = rot;
				
				if (Input.GetKey(KeyCode.W)){
					toMove += this.transform.forward * amount;
				}else if (Input.GetKey(KeyCode.S)){
					toMove -= this.transform.forward * amount;
				}
				
				if (Input.GetKey(KeyCode.A)){
					toMove -= this.transform.right * amount;
				}else if (Input.GetKey(KeyCode.D)){
					toMove += this.transform.right * amount;
				}
				
				if (Input.GetKey(KeyCode.Space)){
					toMove += this.transform.up * amount;
				}else if (Input.GetKey(KeyCode.LeftShift)){
					toMove -= this.transform.up * amount;
				}
				
				if (Input.GetKey(KeyCode.LeftAlt)){
					amount *= 0.3F;
				}
				
				this.transform.position += toMove * Time.deltaTime;
			}else if (Input.GetMouseButtonUp(1)){
				Cursor.lockState = CursorLockMode.None;
				Cursor.visible = true;
			}
		}
	}
}
