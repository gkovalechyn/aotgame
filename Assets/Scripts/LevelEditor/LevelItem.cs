using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SimpleJSON;

public class LevelItem{
	private bool isPrimitive;
	private int primitiveType;

	private string name;

	private bool isPrefab;
	private int placeableObjectId;

	private bool isLight;
	private int lightType;

	private bool collides;

	private float posX;
	private float posY;
	private float posZ;

	private float scaleX;
	private float scaleY;
	private float scaleZ;

	private float qx;
	private float qy;
	private float qz;
	private float qw;

	private string textureURL = "N/A"; //Couldn't figure out a better way to do it
	private string textureName;
	private float textureOffsetX;
	private float textureOffsetY;
	private float textureTilingX;
	private float textureTilingY;

	public LevelItem(PlacedObjectInfo info){
		Vector3 pos = info.transform.position;
		Vector3 scale = info.transform.localScale;
		Quaternion rotation = info.transform.rotation;
		Renderer renderer = info.GetComponent<Renderer>();

        if(renderer != null && renderer.material != null && renderer.material.mainTexture != null) {
            Material material = renderer.material;

            this.textureName = material.mainTexture.name;
            this.textureURL = info.textureURL;
            this.textureOffsetX = material.mainTextureOffset.x;
            this.textureOffsetY = material.mainTextureOffset.y;
            this.textureTilingX = material.mainTextureScale.x;
            this.textureTilingY = material.mainTextureScale.y;
        } else {
            this.textureName = "No texture";
            this.textureURL = "N/A";
            this.textureOffsetX = -1;
            this.textureOffsetY = -1;
            this.textureTilingX = -1;
            this.textureTilingY = -1;
        }

		this.name = info.gameObject.name;

		this.isPrimitive = info.isPrimitive;
		this.isPrefab = info.isPrefab;
		this.isLight = info.isLight;

		this.primitiveType = (int) info.primitiveType;
		this.placeableObjectId = info.placeableObject != null ? (int) info.placeableObject.getId() : -1;
		this.lightType = (int) info.lightType;

		this.posX = pos.x;
		this.posY = pos.y;
		this.posZ = pos.z;

		this.scaleX = scale.x;
		this.scaleY = scale.y;
		this.scaleZ = scale.z;

		this.qx = rotation.x;
		this.qy = rotation.y;
		this.qz = rotation.z;
		this.qw = rotation.w;
	}

	public LevelItem(JSONClass node){
		JSONNode posNode = node["Position"];
		JSONNode scaleNode = node["Scale"];
		JSONNode rotationNode = node["Rotation"];
		JSONNode textureNode = node["Texture"];

		this.name = node["Name"];

		this.isPrimitive = node["IsPrimitive"].AsBool;
		this.isPrefab = node["IsPrefab"].AsBool;
		this.isLight = node["IsLight"].AsBool;

		this.primitiveType = node["PrimitiveType"].AsInt;
		this.placeableObjectId = node["PlaceableObjectID"].AsInt;
		this.lightType = node["LightType"].AsInt;

		this.posX = posNode["x"].AsFloat;
		this.posY = posNode["y"].AsFloat;
		this.posZ = posNode["z"].AsFloat;

		this.scaleX = scaleNode["x"].AsFloat;
		this.scaleY = scaleNode["y"].AsFloat;
		this.scaleZ = scaleNode["z"].AsFloat;

		this.qx = rotationNode["x"].AsFloat;
		this.qy = rotationNode["y"].AsFloat;
		this.qz = rotationNode["z"].AsFloat;
		this.qw = rotationNode["w"].AsFloat;

        if(textureNode != null) {
            this.textureName = textureNode["Name"];
            this.textureURL = textureNode["URL"];
            this.textureOffsetX = textureNode["Offset X"].AsFloat;
            this.textureOffsetY = textureNode["Offset Y"].AsFloat;
            this.textureTilingX = textureNode["Tiling X"].AsFloat;
            this.textureTilingY = textureNode["Tiling Y"].AsFloat;
        }
	}

	public void save(JSONClass node){
		JSONClass posNode = new JSONClass();
		JSONClass scaleNode = new JSONClass();
		JSONClass rotationNode = new JSONClass();
		JSONClass textureNode = new JSONClass();

		node["Name"] = this.name;

		node["IsPrimitive"].AsBool = this.isPrimitive;
		node["IsPrefab"].AsBool = this.isPrefab;
		node["IsLight"].AsBool = this.isLight;
		
		node["PrimitiveType"].AsInt = this.primitiveType;
		node["PlaceableObjectID"].AsInt = this.placeableObjectId;
        node["LightType"].AsInt = (int) this.lightType;

		posNode["x"].AsFloat = this.posX;
		posNode["y"].AsFloat = this.posY;
		posNode["z"].AsFloat = this.posZ;
		
		scaleNode["x"].AsFloat = this.scaleX;
		scaleNode["y"].AsFloat = this.scaleY;
		scaleNode["z"].AsFloat = this.scaleZ;
		
		rotationNode["x"].AsFloat = this.qx;
		rotationNode["y"].AsFloat = this.qy;
		rotationNode["z"].AsFloat = this.qz;
		rotationNode["w"].AsFloat = this.qw;

        if(this.textureURL != "N/A") {
            textureNode["Name"] = this.textureName;
            textureNode["URL"] = this.textureURL;
            textureNode["Offset X"].AsFloat = this.textureOffsetX;
            textureNode["Offset Y"].AsFloat = this.textureOffsetY;
            textureNode["Tiling X"].AsFloat = this.textureTilingX;
            textureNode["Tiling Y"].AsFloat = this.textureTilingY;
        }

		node.Add("Position", posNode);
		node.Add("Scale", scaleNode);
		node.Add("Rotation", rotationNode);

        if(this.textureURL != "N/A") {
            node.Add("Texture", textureNode);
        }
	}


	public PlacedObjectInfo place(bool isInEditor){
		Quaternion rotation = new Quaternion(qx, qy, qz, qw);
		Vector3 pos = new Vector3(posX, posY, posZ);
		Vector3 scale = new Vector3(scaleX, scaleY, scaleZ);
		Light light = null;
		GameObject go = null;
		PlacedObjectInfo info = null;

		if (this.isPrimitive){
			go = GameObject.CreatePrimitive(Utils.primitiveFromInt(this.primitiveType));
		}else if (this.isPrefab){
			go = (GameObject) GameObject.Instantiate(Resources.Load<GameObject>(PlaceableObject.getById(this.placeableObjectId).getPath()));

			if (this.placeableObjectId == PlaceableObject.PLAYER_SPAWNPOINT.getId() ||
			    this.placeableObjectId == PlaceableObject.TITAN_SPAWNPOINT.getId()){

                    if(!isInEditor) {
                        go.SetActive(false);
                    }
			}
		}else if (this.isLight){
			go = new GameObject();
			light = go.AddComponent<Light>();
			light.type = Utils.lightFromInt(this.lightType);

            if(isInEditor) {
                GameObject gizmo = (GameObject) GameObject.Instantiate(Resources.Load<GameObject>(PlaceableObject.LIGHT_GIZMO.getPath()));

                go.AddComponent<BoxCollider>();

                gizmo.transform.position = Vector3.zero;
                gizmo.transform.SetParent(go.transform);
                
            }
			
		}

		go.name = this.name;

		go.transform.position = pos;
		go.transform.localScale = scale;
		go.transform.rotation = rotation;

		info = go.AddComponent<PlacedObjectInfo>();

        info.name = this.name;

		info.isPrimitive = this.isPrimitive;
		info.primitiveType = Utils.primitiveFromInt(this.primitiveType);

		info.isPrefab = this.isPrefab;
		info.placeableObject = PlaceableObject.getById(this.placeableObjectId);

		info.isLight = this.isLight;
		info.lightType = Utils.ligthFromInt(this.lightType);

		if (!this.isPrefab && !this.isPrimitive){
            Renderer renderer = go.GetComponent<Renderer>();
            if(renderer != null) {
                renderer.enabled = false || isInEditor;
            }
		}

		return info;
	}
}


