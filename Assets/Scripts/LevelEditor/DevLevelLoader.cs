using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class DevLevelLoader : LevelLoader{

	override public bool save(string levelName, CustomLevel info){

		if (info != null){
            info.getLevelInfo().SaveToFile(LevelLoader.path + levelName + "/level.json");
			info.getMap().SaveToFile(LevelLoader.path + levelName + "/map.json");
			info.getNavmesh().SaveToFile(LevelLoader.path + levelName + "/navmesh.json");
			info.getTextures().SaveToFile(LevelLoader.path + levelName + "/textures.json");
			return true;
		}else{
			return false;
		}
	}

	override public CustomLevel load(string levelName){
		//LevelLoader.checkPath(levelName);
		CustomLevel level = new CustomLevel();
        JSONClass levelInfo = (JSONClass) JSONClass.LoadFromFile(LevelLoader.path + levelName + "/level.json");
		JSONClass map = (JSONClass) JSONClass.LoadFromFile(LevelLoader.path + levelName + "/map.json");
		JSONClass navmesh = (JSONClass) JSONClass.LoadFromFile(LevelLoader.path + levelName + "/navmesh.json");
		JSONClass textures = (JSONClass) JSONClass.LoadFromFile(LevelLoader.path + levelName + "/textures.json");
		
		JSONArray levelItems = map["Items"].AsArray;
        Debug.Log("DevLevelLoader::load: Found " + levelItems.Count + " items.");
		LevelItem[] items = new LevelItem[levelItems.Count];

		for(int i = 0; i < items.Length; i++){
			items[i] = new LevelItem((JSONClass) levelItems[i]);
		}

		level.setItems(items);
		level.setLevelInfo(levelInfo);
		level.setNavmesh(navmesh);
		level.setTextures(textures);

		return level;
	}

}

