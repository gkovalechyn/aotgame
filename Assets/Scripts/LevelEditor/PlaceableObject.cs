using UnityEngine;
using System.Collections;

public class PlaceableObject{

	public static readonly PlaceableObject TITAN_DUMMY = new PlaceableObject(0, "Titan dummy", "TitanDummy");
	public static readonly PlaceableObject PLAYER_SPAWNPOINT = new PlaceableObject(1, "Player spawnpoint", "PlayerSpawnpoint");
	public static readonly PlaceableObject TITAN_SPAWNPOINT = new PlaceableObject(2, "Titan spawnpoint", "TitanSpawnpoint");
	public static readonly PlaceableObject LIGHT_GIZMO = new PlaceableObject(3, "Light gizmo", "LightGizmo");

	private readonly int id;
	private readonly string name;
	private readonly string path;

	private static readonly PlaceableObject[] _values = new PlaceableObject[]{
		TITAN_DUMMY,
		PLAYER_SPAWNPOINT,
		TITAN_SPAWNPOINT
	};

	private PlaceableObject(int id, string prefabName, string prefabPath){
		this.id = id;
		this.path = prefabPath;
		this.name = prefabName;
	}

	public string getPath(){
		return this.path;
	}

	public string getName(){
		return this.name;
	}

	public int getId(){
		return this.id;
	}

	public static PlaceableObject[] values(){
		return PlaceableObject._values;
	}

	public static PlaceableObject getById(int id){
		foreach(PlaceableObject po in PlaceableObject._values){
			if (po.getId() == id){
				return po;
			}
		}

		return null;
	}
}

