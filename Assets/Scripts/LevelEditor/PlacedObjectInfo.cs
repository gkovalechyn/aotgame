using UnityEngine;
using System.Collections;

public class PlacedObjectInfo : MonoBehaviour{
	public bool isPrimitive;
	public PrimitiveType primitiveType;

	public bool isPrefab;
	public PlaceableObject placeableObject;

	public bool isLight;
	public LightType lightType;

	public bool collides;

	public string textureURL;

}

