using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using SimpleJSON;

public abstract class LevelLoader{
	//SavedLevels/<LevelName>/map.json
	//SavedLevels/<LevelName>/navmesh.json
	//SavedLevels/<LevelName>/textures/
	//SavedLevel/<LevelName>/textures.json
	//SavedLevels/<LevelName>/scripts/

	public const string path = "SavedLevels/";
	private static Dictionary<string, LevelLoader> levelLoaders = new Dictionary<string, global::LevelLoader>();

	static LevelLoader(){
		levelLoaders[MainGameController.VERSION] = new DevLevelLoader();
	}

	public abstract bool save(string levelName, CustomLevel info);

	public abstract CustomLevel load(string levelName);

	public static bool saveLevel(string levelName, CustomLevel info){
		LevelLoader loader = null;
		LevelLoader.levelLoaders.TryGetValue(MainGameController.VERSION, out loader);

		if (loader != null){
			return loader.save(levelName, info);
		}else{
			throw new System.Exception("This game version \"" + MainGameController.VERSION + "\" does not support saving/loading levels.");
		}
	}

	public static CustomLevel loadLevel(string levelName){
		LevelLoader loader = null;
		LevelLoader.levelLoaders.TryGetValue(MainGameController.VERSION, out loader);
		
		if (loader != null){
			return loader.load(levelName);
		}else{
			throw new System.Exception("This game version \"" + MainGameController.VERSION + "\" does not support saving/loading levels.");
		}
	}

	public static void checkPath(string path){
		string fullpath = LevelLoader.path + path + "/";
		if (!Directory.Exists(fullpath)){
			Directory.CreateDirectory(fullpath);
		}
	}

	public static List<LevelItem> getLevelItems(){
		PlacedObjectInfo[] placedObjects = GameObject.FindObjectsOfType<PlacedObjectInfo>();
		List<LevelItem> items = new List<LevelItem>(placedObjects.Length);

		foreach(PlacedObjectInfo info in placedObjects){
			items.Add(new LevelItem(info));
		}

		return items;
	}
}