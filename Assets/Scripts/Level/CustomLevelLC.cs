using UnityEngine;
using SimpleJSON;
using System.Collections;
using System.Collections.Generic;

public class CustomLevelLC : LevelController{
	private LevelItem[] levelItems = null;
	private IList<Vector3> playerSpawns = new List<Vector3>();
	private IList<Vector3> titanSpawns = new List<Vector3>();

    private LinkedList<LevelItem> spawnableItems = new LinkedList<LevelItem>();
    private LinkedList<PlacedObjectInfo> spawnedItems = new LinkedList<PlacedObjectInfo>();

	private Dictionary<string, Material> loadedMaterials = new Dictionary<string, Material>();
	private Shader standartShader = Shader.Find("Standart");

	private JSONClass level;

	private bool isPaused = false;
	private string levelName;

    private AIManager aiManager;
	// Use this for initialization
	void Start (){
		this.levelName = MainGameController.Instance.getData(CustomDataName.CUSTOM_LEVEL_LOAD);

        if(this.levelName == null) {
            MainGameController.Instance.loadLevel(Level.MENU);
            return;
        }

        MainGameController.Instance.setData(CustomDataName.CUSTOM_LEVEL_LOAD, null);
        aiManager = new AIManager();

		this.loadLevel();
	}

	// Update is called once per frame
	public void Update (){

	}

	public override void tick () {

	}
	public override void end () {
		//nothing.
	}

	public override void reload () {
		//REDO: Cache everything, right now it's just for testing purposes.
		foreach(PlacedObjectInfo info in this.spawnedItems){
			GameObject.Destroy(info.transform.gameObject);
		}

        this.spawnedItems.Clear();

		GameObject.Destroy(CCharacterController.current.transform.root.gameObject);
		CCharacterController.current = null;

		foreach(LevelItem item in this.spawnableItems){
			this.spawnedItems.AddLast(item.place(false));
		}

        this.spawnPlayer();
	}

	private void spawnPlayer(){
		Vector3 pos = this.playerSpawns[UnityEngine.Random.Range(0, this.playerSpawns.Count)];
		GameObject resource = Resources.Load<GameObject>(ResourcePath.PLAYER.getPath());
		GameObject.Instantiate(resource, pos, Quaternion.identity);
	}

	public override void onEscPressed () {
		this.isPaused = !this.isPaused;

		if (this.isPaused){
			//Enable UI and disable controls
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;
		}else{
			//Disable UI and enable controls
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
		}
	}

	public override void onWindowResize () {
		//nothing.
	}

	private void loadLevel(){
		CustomLevel level = LevelLoader.loadLevel(this.levelName);
		LevelItem[] items = level.getItems();
		PlacedObjectInfo[] go = new PlacedObjectInfo[items.Length];

        this.playerSpawns.Clear();
        this.titanSpawns.Clear();
        this.spawnableItems.Clear();

		for(int i = 0; i < items.Length; i++){
			go[i] = items[i].place(false);
            
            if(go[i].isPrefab) {
                if(go[i].placeableObject == PlaceableObject.PLAYER_SPAWNPOINT) {
                    this.playerSpawns.Add(go[i].transform.position);
                }else if(go[i].placeableObject == PlaceableObject.TITAN_SPAWNPOINT) {
                    this.titanSpawns.Add(go[i].transform.position);
                } else if (go[i].placeableObject == PlaceableObject.LIGHT_GIZMO){
                    this.spawnedItems.AddLast(go[i]);
                    this.spawnableItems.AddLast(levelItems[i]);
                } else if(go[i].placeableObject == PlaceableObject.TITAN_DUMMY) {
                    this.spawnedItems.AddLast(go[i]);
                    this.spawnableItems.AddLast(levelItems[i]);
                }
            } else {
                this.spawnedItems.AddLast(go[i]);
                this.spawnableItems.AddLast(levelItems[i]);
            }
		}

		this.levelItems = items;

        Debug.Log("Found " + this.playerSpawns.Count + " player spawns.");
        Debug.Log("Found " + this.titanSpawns.Count + " titan spawns.");
	}

	public Material getMaterial(string textureName){
		Material mat = null;
		this.loadedMaterials.TryGetValue(textureName, out mat);

		if (mat == null){
			WWW www = new WWW("file://./SavedLevels/" + MainGameController.Instance.getCustomLevelToLoad() + "/Textures/" + textureName);
			Texture texture = (Texture) www.texture;
			mat = new Material(this.standartShader);
			mat.mainTexture = texture;

			this.loadedMaterials[textureName] = mat;
		}

		return mat;
	}	
}

