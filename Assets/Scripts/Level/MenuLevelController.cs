using UnityEngine;
using System.Collections;

public class MenuLevelController : LevelController{

	public void Start(){
		DebugMenu.log("Loaded level " + MainGameController.Instance.CurrentLevel.getName());

		Cursor.lockState = CursorLockMode.None;
	}

	public override void tick() {
		//do nothing
	}

	public void OnDestroy() {
		DebugMenu.log("Ending MenuLevelController");
	}

	public override void end () {

	}

	public override void reload() {
		DebugMenu.log("Reloading MenuLevelController");
	}

	public override void onEscPressed(){
		//@TODO Toggle the menu GUI
	}

	public override void onWindowResize () {
		//this.updateRects();
	}

	private void updateRects(){
		int rectInterval = 15;
		int rectCount = 5;

		//int width = (int) Mathf.Clamp(this.oldWidth * 0.8f, 100f, 300f);
		int height = 30;
		//int left = (int) this.oldWidth / 2 - width / 2;
		int top = (Screen.height / 2);

		//Shouldn't it be top-= ? It's weird but it works.
		if (rectCount % 2 == 0){
			top += (-rectInterval / 2) - ((rectCount / 2) * height) - (rectInterval * ((rectCount / 2) - 1));
		}else{
			//top += (height / 2) - ((rectCount / 2) * rectInterval);
			//top += (-rectInterval / 2) - ((rectCount / 2) * height) - (rectInterval * ((rectCount / 2) - 1));
			top += (-height / 2) - ((rectCount / 2) * rectInterval) - ((int) (rectCount / 2)) * height;

		}

		//this.level1Rect = new Rect(left, top, width, height);
		//this.level2Rect = new Rect(left, top + (height + rectInterval), width, height);
		//this.levelEditorRect = new Rect(left, top + (height + rectInterval) * 2, width, height);
		//this.optionsRect = new Rect(left, top + (height + rectInterval) * 3, width, height);
		//this.quitRect = new Rect(left, top + (height + rectInterval) * 4, width, height);
	}

}

