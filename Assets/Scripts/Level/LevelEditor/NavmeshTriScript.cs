using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class NavmeshTriScript : MonoBehaviour{
	public List<NavmeshVertexScript> vertices = new List<NavmeshVertexScript>(3);
	private MeshFilter meshFilter;
	public float cost = 1F;
	public float maxSize = 15F;

	//Info displayer
	//private GameObject canvas;
	//private Text label;

	public void Awake(){
		this.meshFilter = this.GetComponent<MeshFilter>();
		//this.canvas = GameObject.Instantiate(Resources.Load<GameObject>("Prefabs/3DText"));
		//this.label = this.canvas.GetComponentInChildren<Text>();
	}

	public void updadeMesh(){
		if (this.vertices.Count == 3){
			Vector3 position = Vector3.zero;
			Mesh mesh = this.meshFilter.mesh;
			mesh.triangles = new int[0];
			mesh.vertices = new Vector3[]{vertices[0].transform.position, vertices[1].transform.position, vertices[2].transform.position};
			mesh.triangles = new int[]{0,1,2,2,1,0};

			//this.canvas.transform.position = (mesh.vertices[0] + mesh.vertices[1] + mesh.vertices[2]) / 3;
			//mesh.Optimize();
			//mesh.RecalculateNormals();
		}
	}

	public void removeReferences(){
		foreach(NavmeshVertexScript ver in this.vertices){
			ver.tris.Remove(this);
			ver.deleteIfNotNeeded();
		}
	}

    public Vector3 getNormal() {
        Vector3 v1 = this.vertices[0].transform.position;
        Vector3 v2 = this.vertices[1].transform.position;
        Vector3 v3 = this.vertices[2].transform.position;
        Vector3 normal = new Vector3();

        normal = Vector3.Cross(v2 - v1, v3 - v1).normalized;

        if(Vector3.Angle(normal, Vector3.up) > 90F) {
            normal *= -1F;
        }

        return normal;
    }
}

