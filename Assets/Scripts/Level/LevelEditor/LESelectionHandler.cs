﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
class LESelectionHandler : MonoBehaviour{
    public List<GameObject> primitives = new List<GameObject>();
    public List<GameObject> prefabs = new List<GameObject>();
    public List<GameObject> specials = new List<GameObject>();
    public void Start() {
        this.toggleList(this.primitives, true);
        this.toggleList(this.prefabs, false);
        this.toggleList(this.specials, false);
    }

    public void onPrimitivesClicked() {
        this.toggleList(this.primitives, true);
        this.toggleList(this.prefabs, false);
        this.toggleList(this.specials, false);
    }

    public void onPrefabsClicked() {
        this.toggleList(this.primitives, false);
        this.toggleList(this.prefabs, true);
        this.toggleList(this.specials, false);
    }

    public void onSpecialsClicked() {
        this.toggleList(this.primitives, false);
        this.toggleList(this.prefabs, false);
        this.toggleList(this.specials, true);
    }

    private void toggleList(List<GameObject> list, bool enabled) {
        foreach(GameObject go in list) {
            go.SetActive(enabled);
        }
    }
}