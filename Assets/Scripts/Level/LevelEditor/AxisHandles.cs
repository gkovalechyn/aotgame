using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AxisHandles : MonoBehaviour{
	private List<GameObject> targets = new List<GameObject>();

	public Collider xCollider;
	public Collider yCollider;
	public Collider zCollider;

	public float damp = 0.2F;

	private Vector3 movementDirection;

	public bool isDragging;
	public bool isWorldSpace = false;

	// Use this for initialization
	void Start (){
		this.xCollider.GetComponent<Renderer>().material.renderQueue = int.MaxValue;
        this.yCollider.GetComponent<Renderer>().material.renderQueue = int.MaxValue;
        this.zCollider.GetComponent<Renderer>().material.renderQueue = int.MaxValue;
	}

	// Update is called once per frame
	void Update (){
		if (this.targets.Count == 0){
			return;
		}else{
			Vector3 pos = Vector3.zero;

			foreach(GameObject go in this.targets){
				pos += go.transform.position;
			}

			this.transform.position = pos / this.targets.Count;

			if (!this.isWorldSpace && this.targets.Count == 1){
				this.transform.rotation = this.targets[0].transform.rotation;
			}else{
				this.transform.rotation = Quaternion.identity;
			}
		}

		if (this.isDragging){
			float dm = Input.GetAxis("Mouse X")  * Options.mouseSensitivityX + Input.GetAxis("Mouse Y") * Options.mouseSensitivityY;

			if (Input.GetKey(KeyCode.LeftShift)){
				dm *= this.damp;
			}

			foreach(GameObject go in this.targets){
				go.transform.position += this.movementDirection * dm;
			}

			if (Input.GetMouseButtonUp(0)){
				this.isDragging = false;
			}
		}
	}

	public void setTargets(List<GameObject> targets){
		Vector3 pos = Vector3.zero;

		if (targets == null){
			return;
		}

		foreach(GameObject go in targets){
			pos += go.transform.position;
		}

		pos /= targets.Count;

		this.transform.localScale = Vector3.one;

		this.targets = targets;
	}

	public bool checkHit(){
		if (Input.GetMouseButtonDown(0)){
			Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);
			foreach(RaycastHit hit in Physics.RaycastAll(r, Vector3.Distance(Camera.main.transform.position, this.transform.position))){
				if (hit.collider == this.xCollider){
					this.movementDirection = ((this.isWorldSpace) ? Vector3.right : this.transform.right);
					this.isDragging = true;
					return true;
				}else if (hit.collider == this.yCollider){
					this.movementDirection = ((this.isWorldSpace) ? Vector3.up : this.transform.up);
					this.isDragging = true;
					return true;
				}else if (hit.collider == this.zCollider){
					this.movementDirection = ((this.isWorldSpace) ? Vector3.forward : this.transform.forward);
					this.isDragging = true;
					return true;
				}
			}
		}
		return false;
	}
}

