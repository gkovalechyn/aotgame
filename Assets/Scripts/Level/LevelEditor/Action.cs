using UnityEngine;
using System.Collections;

public interface Action{

	void undo();

	void redo();
}

