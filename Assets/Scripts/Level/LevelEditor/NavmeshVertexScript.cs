using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NavmeshVertexScript : MonoBehaviour{
	public List<NavmeshTriScript> tris = new List<NavmeshTriScript>(4);
	private Vector3 oldPosition;
	public int index;

	public void Start(){
		this.oldPosition = this.transform.position;
	}

	public void Update(){
		if (this.transform.position != this.oldPosition){
			foreach(NavmeshTriScript tri in this.tris){
				tri.updadeMesh();
			}
		}
	}

	public void deleteIfNotNeeded(){
		if (this.tris.Count == 0){
			GameObject.Destroy(this.transform.root.gameObject);
		}
	}

	public void delete(){
		NavmeshTriScript[] tris = new NavmeshTriScript[this.tris.Count];
		this.tris.CopyTo(tris);
		this.tris.Clear();
		foreach(NavmeshTriScript tri in tris){
			tri.removeReferences();
			GameObject.Destroy(tri.transform.root.gameObject);
		}
		GameObject.Destroy(this.transform.root.gameObject);
	}
}

