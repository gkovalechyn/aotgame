﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class LESelectableObjectScript : MonoBehaviour{
    public bool isPrefab;
    public int placeableObject;

    public bool isPrimitive;
    public PrimitiveType primitiveType;

    public bool isSpecial;
    public LightType lightType;

    private LevelEditor2LC levelController;
    public void Start() {
        levelController = (LevelEditor2LC) MainGameController.Instance.getLevelController();
    }

    public void onClick() {
        if (this.isPrefab){
            levelController.createObject(PlaceableObject.getById(this.placeableObject));
        }else if (this.isPrimitive){
            this.levelController.createObject(this.primitiveType);
        }else if (this.isSpecial){
            this.levelController.createObject(this.lightType);
        }
        
    }
}

