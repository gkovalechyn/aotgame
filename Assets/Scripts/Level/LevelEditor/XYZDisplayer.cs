using UnityEngine;
using System.Collections;

public class XYZDisplayer : MonoBehaviour{
	private LineRenderer xRenderer;
	private LineRenderer yRenderer;
	private LineRenderer zRenderer;

	private Material shaderMaterial;

	public float size = 1F;

	public void Start(){
		GameObject xPlaceholder = new GameObject();
		GameObject yPlaceholder = new GameObject();
		GameObject zPlaceholder = new GameObject();

		xPlaceholder.name = "xLineRenderer";
		yPlaceholder.name = "yLineRenderer";
		zPlaceholder.name = "zLineRenderer";

		this.shaderMaterial = new Material(Shader.Find("Particles/Additive"));

		xPlaceholder.transform.parent = this.transform;
		yPlaceholder.transform.parent = this.transform;
		zPlaceholder.transform.parent = this.transform;

		xRenderer = xPlaceholder.AddComponent<LineRenderer>();
		yRenderer = yPlaceholder.AddComponent<LineRenderer>();
		zRenderer = zPlaceholder.AddComponent<LineRenderer>();

		xRenderer.SetVertexCount(2);
		yRenderer.SetVertexCount(2);
		zRenderer.SetVertexCount(2);

		xRenderer.SetColors(Color.red, Color.red);
		yRenderer.SetColors(Color.green, Color.green);
		zRenderer.SetColors(Color.blue, Color.blue);

		xRenderer.SetWidth(0.1F, 0.1F);
		yRenderer.SetWidth(0.1F, 0.1F);
		zRenderer.SetWidth(0.1F, 0.1F);

		xRenderer.material = this.shaderMaterial;
		yRenderer.material = this.shaderMaterial;
		zRenderer.material = this.shaderMaterial;
	}


	public void Update(){
		Vector3 thisPos = this.transform.position;
		this.xRenderer.SetPosition(0, thisPos);
		this.xRenderer.SetPosition(1, thisPos + new Vector3(this.size, 0F, 0F));

		this.yRenderer.SetPosition(0, thisPos);
		this.yRenderer.SetPosition(1, thisPos + new Vector3(0F, this.size, 0F));

		this.zRenderer.SetPosition(0, thisPos);
		this.zRenderer.SetPosition(1, thisPos + new Vector3(0F, 0F, this.size));
	}
}

