using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelEditorClickHandler : MonoBehaviour{
	private LevelEditor2LC lc;

	private Animator objectPanelAnimator;
	private Text hidePanelButtonText;
	private bool objectPanelHidden = false;

	public Text levelNameText;

	public void Start(){
		this.lc = GameObject.FindObjectOfType<LevelEditor2LC>();

		this.objectPanelAnimator = GameObject.Find("ObjectInfoPanel").GetComponent<Animator>();
		this.hidePanelButtonText = GameObject.Find("HideButton").GetComponentInChildren<Text>();
	}

	public void onScaleEdit(){
		lc.updateScale();
	}

	public void onPositionEdit(){
		lc.updatePosition();
	}

	public void onRotationEdit(){
		lc.updateRotation();
	}

	public void onMovementAmountChange(){

	}

	public void onTextureSelect(){

	}

	public void onMoveScriptToggle(){

	}

	public void onRotateScriptToggle(){

	}

	public void onSaveClicked(){
		string text = this.levelNameText.text;

		if (!string.IsNullOrEmpty(text)){
			LevelLoader.saveLevel(text, this.lc.getCustomLevel());
		}
	}

	public void onLoadClicked(){
		string text = this.levelNameText.text;
		
		if (!string.IsNullOrEmpty(text)){
			this.lc.loadLevel(text);
		}
	}

	public void onClearClicked(){
		foreach(PlacedObjectInfo info in GameObject.FindObjectsOfType<PlacedObjectInfo>()){
			GameObject.Destroy(info.gameObject);
		}
	}

	public void onSettingsClicked(){
		Debug.Log("@TODO Finish the settings button.");
	}

	public void onNavmeshClicked(){
		if (this.lc.getCurrentMode() == LevelEditor2LC.Mode.NAVMESH){
			this.lc.setMode(LevelEditor2LC.Mode.EDIT);
		}else{
			this.lc.setMode(LevelEditor2LC.Mode.NAVMESH);
		}
	}

	public void onGridClicked(){
		Debug.Log("@TODO Finish the grid button.");
	}

	public void onExitClicked(){
		MainGameController.Instance.loadLevel(Level.MENU);
	}

	public void onPanelToggleClick(){
		this.objectPanelHidden = !this.objectPanelHidden;

		if (this.objectPanelHidden){
			this.hidePanelButtonText.text = "<<";
		}else{
			this.hidePanelButtonText.text = ">>";
		}
		this.objectPanelAnimator.SetBool("Hidden", this.objectPanelHidden);
	}
}

