//------------------------------------------------------------------------------
// <auto-generated>
//     O código foi gerado por uma ferramenta.
//     Versão de Tempo de Execução:4.0.30319.34014
//
//     As alterações ao arquivo poderão causar comportamento incorreto e serão perdidas se
//     o código for gerado novamente.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;

public class Level{
	//Instances
	public static readonly Level MENU = new Level(0, "Menu", typeof(MenuLevelController));
	public static readonly Level AOT_GAME = new Level(1, "AoTGame", typeof(AoTGameLevelController));
	public static readonly Level TITAN_DUMMY_TEST = new Level(2, "TitanDummyLevel", typeof(TitanDummyLevelController));
	public static readonly Level COURSE_3DMG = new Level(5, "3dmgCourse", null);
	public static readonly Level CUSTOM_LEVEL = new Level(3, "CustomLevel", typeof(CustomLevelLC));
	public static readonly Level LEVEL_EDITOR = new Level(6, "LevelEditor2", typeof(LevelEditor2LC));
	//Fields
	private readonly string name;
	private readonly int levelID;
	private readonly Type levelController;

	public static readonly Level[] values = new Level[]{
		Level.MENU,
		Level.AOT_GAME,
		Level.TITAN_DUMMY_TEST,
		Level.COURSE_3DMG,
		Level.CUSTOM_LEVEL,
		Level.LEVEL_EDITOR
	};
	private Level(int id, string levelName, Type script){
		this.levelID = id;
		this.name = levelName;
		this.levelController = script;
	}

	public static Level getByName(string level){
		foreach (Level l in Level.values){
			if (l.getName().Equals(level)){
				return l;
			}
		}

		return null;
	}

	/*
	public static IEnumerable<Level> values{
		get{
			yield return MENU;
			yield return AOT_GAME;
			yield return TITAN_DUMMY_TEST;
			yield return COURSE_3DMG;
			yield return LEVEL_EDITOR;
			yield return CUSTOM_LEVEL;
		}
	}
	*/

	public static Level getByID(int id){
		foreach(Level level in Level.values){
			if (level.getID() == id){
				return level;
			}
		}

		return null;
	}

	public string getName(){
		return this.name;
	}

	public int getID(){
		return this.levelID;
	}

	public Type getController(){
		return this.levelController;
	}

}

