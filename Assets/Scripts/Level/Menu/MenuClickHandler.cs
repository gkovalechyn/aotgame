using UnityEngine;
using System.Collections;

public class MenuClickHandler : MonoBehaviour{
	private MenuClickHandler.State state = MenuClickHandler.State.MENU;
	private Animator menuAnimator;
	private Animator optionsAnimator;

	public void Start(){
		this.menuAnimator = GameObject.Find("MainPanel").GetComponent<Animator>();
		this.optionsAnimator = GameObject.Find("OptionsPanel").GetComponent<Animator>();
	}

	public void onLoadOldLevelClick(){
		MainGameController.Instance.loadLevel(Level.AOT_GAME);
	}

	public void onLoadNewLevelClick(){
		MainGameController.Instance.loadLevel(Level.TITAN_DUMMY_TEST);
	}

	public void onOptionsClickToggle(){
		if (this.state == State.MENU){
			this.state = State.OPTIONS;
			menuAnimator.SetBool("Visible", false);
			optionsAnimator.SetBool("Visible", true);
		}else{
			this.state = State.MENU;
			menuAnimator.SetBool("Visible", true);
			optionsAnimator.SetBool("Visible", false);
			//Options.save();
		}
	}

	public void onLevelEditorClick(){
		MainGameController.Instance.loadLevel(Level.LEVEL_EDITOR);
	}

	public void onExitClick(){
		Options.save();
		MainGameController.Instance.exitGame();
	}

	private enum State{
		MENU,
		OPTIONS
	}
}

