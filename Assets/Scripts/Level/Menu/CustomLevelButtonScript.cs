﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CustomLevelButtonScript : MonoBehaviour {
	public void onClick(){
		MainGameController.Instance.setData(CustomDataName.CUSTOM_LEVEL_LOAD, this.GetComponentInChildren<Text>().text);
		MainGameController.Instance.loadLevel(Level.CUSTOM_LEVEL);
	}
}
