using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class CustomLevelPanelController : MonoBehaviour{
	public GameObject buttonPrefab;

	public void OnEnable(){
		string[] folders = Directory.GetDirectories("./" + LevelLoader.path);
		string[] parts;
		int i = 0;

		if (folders.Length == 0){
			return;
		}

		foreach(Transform tf in this.transform){
			if (i < folders.Length){
				tf.gameObject.SetActive(true);
				Text text = tf.GetComponentInChildren<Text>();
				parts = folders[i].Split('/');
				text.text = parts[parts.Length - 1];
				i++;
			}else{
				tf.gameObject.SetActive(false);
			}
		}

		while(i < folders.Length){
			GameObject go = GameObject.Instantiate(buttonPrefab);
			go.transform.SetParent(this.transform);
			parts = folders[i].Split('/');
			go.GetComponentInChildren<Text>().text = parts[parts.Length - 1];
			i++;
		}
	}

	public void OnDisable(){

	}
}