using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OptionsEventHandler : MonoBehaviour{
	//FoV
	public Slider fovSlider;
	public Text fovLabel;

	//Graphics
	public Text shadowDistanceInput;
	public Toggle softVegetationToggle;
	public Toggle realtimeReflectionProbesToggle;
	public Text drawDistanceInput;
	//Sound
	public Slider soundSlider;
	public Slider musicSlider;

	public Text mouseXSensitivityInput;
	public Text mouseYSensitivityInput;
	public Toggle invertMouseXToggle;
	public Toggle invertMouseYToggle;
	// Use this for initialization
	void Start (){
        this.updateDisplayValues();
	}

    public void updateDisplayValues() {
        this.fovSlider.value = Options.fov;
        this.fovLabel.text = "" + Options.fov;

        this.shadowDistanceInput.text = "" + Options.shadowDistance;
        this.softVegetationToggle.isOn = Options.softVegetation;
        this.realtimeReflectionProbesToggle.isOn = Options.realtimeReflectionProbes;
        this.drawDistanceInput.text = "" + Options.drawDistance;

        this.mouseXSensitivityInput.text = "" + Mathf.Abs(Options.mouseSensitivityX);
        this.mouseYSensitivityInput.text = "" + Mathf.Abs(Options.mouseSensitivityY);

        this.invertMouseXToggle.isOn = Options.mouseSensitivityX < 0;
        this.invertMouseYToggle.isOn = Options.mouseSensitivityY < 0;
    }

	public void onFoVChange(){
		Camera.main.fieldOfView = this.calculateFoV(this.fovSlider.value);
		fovLabel.text = "" + ((int)fovSlider.value);
	}

	public void onMouseSensitivityChanged(){
		Options.mouseSensitivityX = this.invertMouseXToggle.isOn ? float.Parse(this.mouseXSensitivityInput.text) : - float.Parse(this.mouseXSensitivityInput.text);
		Options.mouseSensitivityY = this.invertMouseYToggle.isOn ? float.Parse(this.mouseYSensitivityInput.text) : - float.Parse(this.mouseYSensitivityInput.text);
	}

	public void onMouseInvertToggled(){
		if (this.invertMouseYToggle.isOn && Options.mouseSensitivityY > 0){
			Options.mouseSensitivityY *= -1;
		}else if (Options.mouseSensitivityY < 0){
			Options.mouseSensitivityY *= -1;
		}

		if (this.invertMouseXToggle.isOn && Options.mouseSensitivityX > 0){
			Options.mouseSensitivityX *= -1;
		}else if (Options.mouseSensitivityX < 0){
			Options.mouseSensitivityX *= -1;
		}
	}

	public void onShadowDistanceChanged(){
		Options.shadowDistance = float.Parse(this.shadowDistanceInput.text);
	}

	public void onSoftVegetationToggled(){
		Options.softVegetation = this.softVegetationToggle.isOn;
	}

	public void onRealtimeReflectionProblesToggled(){
		Options.realtimeReflectionProbes = this.realtimeReflectionProbesToggle.isOn;
	}

	public void onDrawDistanceChanged(){
		Options.drawDistance = float.Parse(this.drawDistanceInput.text);
	}

	public void onSoundChange(){
		Options.soundVolume = soundSlider.value;
	}

	public void onMusicChange(){
		Options.musicVolume = soundSlider.value;
	}

	private float calculateFoV(float targetFoV){
		float hFovRad = targetFoV * Mathf.Deg2Rad;
		float camH = Mathf.Tan(hFovRad * 0.5F) / Options.aspectRatio;
		return (Mathf.Atan(camH) * 2F) * Mathf.Rad2Deg;
	}

	public void onExitClicked(){

	}

	public void onOkClicked(){
        Debug.Log("Saving values");
		Options.save();
	}

}

