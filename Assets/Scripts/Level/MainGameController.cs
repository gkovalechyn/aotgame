﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public sealed class MainGameController : MonoBehaviour {
	public const string VERSION = "0.1a";
	private static MainGameController instance;
	private static GameObject gameControllerObject;

	private Level currentLevel = null;
	private LevelController currentLevelController = null;
	private GameObject levelControllerObject = null;

	private AIManager aiManager;

	private IList<UIElement> uiElements = new List<UIElement>();

	private string customLevelName;
    private Dictionary<string, string> customData;
	public void Awake(){
        customData = new Dictionary<string, string>();

		Options.load();
	}

	public void Start(){
		MainGameController.checkInstance();

        Options.load();

		DontDestroyOnLoad(gameControllerObject);

		this.uiElements.Add(new TopMenuLabel());

		this.aiManager = new AIManager();
	}

	public void Update(){
		if (Input.GetKeyDown(KeyCode.Escape)){
			if (this.currentLevelController != null){
				this.currentLevelController.onEscPressed();
			}
		}
	}

	public void OnLevelWasLoaded(int levelID){
		this.currentLevel = Level.getByID(levelID);

		this.setupGameQuality();

		this.createTheLevelController();
	}

	public void OnGUI(){
		foreach(UIElement uie in this.uiElements){
			if (uie.toRemove()){
				this.uiElements.Remove(uie);
				continue;
			}

			uie.draw();
		}
	}

	public void loadLevel(Level level){
		if (this.currentLevel != null){
			this.currentLevelController.end();
			this.aiManager.cleanup();
		}

		Application.LoadLevel(level.getName());
	}

	private void tickController(){
		//the currentLevelController never should be null, only in the first level loaded
		try{
			this.currentLevelController.tick();
		}catch(System.Exception e){
			DebugMenu.log("An exception occured on the controller tick. Check the log for more information.");
			Debug.LogException(e);

			if (this.currentLevelController == null){
				throw new System.Exception("Level controller is null for level: " + MainGameController.Instance.currentLevel);
			}

			DebugMenu.log("Reloading the level.");
			this.currentLevelController.reload();
		}
	}

	public static MainGameController Instance{
		get{
			MainGameController.checkInstance();
			return MainGameController.instance;
		}
	}

	private static void checkInstance(){
		if (MainGameController.instance == null){
			MainGameController.gameControllerObject = new GameObject("Game controller");
			MainGameController.instance = MainGameController.gameControllerObject.AddComponent<MainGameController>();

			//MainGameController.gameControllerObject.AddComponent<DebugMenu>();
			instance.uiElements.Add(new DebugMenu());

			instance.currentLevel = Level.getByName("Menu");
			instance.createTheLevelController();
		}
	}

	private void setupGameQuality(){
		Application.targetFrameRate = Options.framerateCap;
		QualitySettings.vSyncCount = Options.vSyncLevel;
		QualitySettings.realtimeReflectionProbes = Options.realtimeReflectionProbes;
		QualitySettings.shadowDistance = Options.shadowDistance;
		QualitySettings.softVegetation = Options.softVegetation;

		if (Options.antiAliasingType == 0){
			QualitySettings.antiAliasing = Options.antiAliasing;
		}else if (Options.antiAliasingType == 1){
			Camera.main.gameObject.AddComponent<FXAA>();
		}

		Camera.main.depth = Options.drawDistance;
	}

	private void createTheLevelController(){
		if (this.currentLevel != null){

			if(this.currentLevelController == null){
				this.InvokeRepeating("tickController", 2.0f, 2.0f);
			}

			this.levelControllerObject = new GameObject();
			this.levelControllerObject.name = "LevelController";
			this.currentLevelController = (LevelController) this.levelControllerObject.AddComponent(this.currentLevel.getController());
		}else{
			throw new System.Exception("Cannot create the LevelController for a null level");
		}
	}

    public string getData(string path) {
        if(this.customData.ContainsKey(path)) {
            return this.customData[path];
        }

        return null;
    }

    public string getData(CustomDataName name) {
        if(this.customData.ContainsKey(name.ToString())) {
            return this.customData[name.ToString()];
        }

        return null;
    }

    public void setData(string path, string value) {
        if(value == null && this.customData.ContainsKey(path)) {
                this.customData.Remove(path);
        } else {
            this.customData[path] = value;
        }
    }

    public void setData(CustomDataName path, string value) {
        if(value == null && this.customData.ContainsKey(path.ToString())) {
            this.customData.Remove(path.ToString());
        } else {
            this.customData[path.ToString()] = value;
        }
    }

	public Level CurrentLevel{
		get{
			return this.currentLevel;
		}
	}

	public LevelController getLevelController(){
		return this.currentLevelController;
	}

	private class TopMenuLabel : UIElement{
		MainGameController controller;
		private Rect rect;

		public TopMenuLabel(){
			controller = MainGameController.Instance;
			rect = new Rect(Screen.width / 2 - 20, 0, 200, 20);
		}

		public override void draw () {
			string text = "Current level: " + controller.currentLevel.getName();
			GUI.Label(this.rect, text);
		}
	}

	public void exitGame(){
		this.currentLevelController.end();

		Application.Quit();
	}

}
