using UnityEngine;
using System.Collections;

public class AoTGameLevelController : LevelController{

	// Use this for initialization
	void Start (){
		DebugMenu.log("Loaded level " + MainGameController.Instance.CurrentLevel.getName());
	}

	public override void tick () {
		//nothing
	}

	public override void end () {
		//nothing
	}

	public override void reload () {
		//nothing
	}

	public override void onEscPressed(){
		//@TODO Toggle the menu GUI
	}

	public override void onWindowResize () {

	}
}

