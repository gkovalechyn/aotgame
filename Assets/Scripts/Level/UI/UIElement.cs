using UnityEngine;
using System.Collections;

public abstract class UIElement{
	private bool _toRemove = false;

	public void remove(){
		this._toRemove = true;
	}

	public bool toRemove(){
		return this._toRemove;
	}

	public abstract void draw();

}

