using UnityEngine;
using System.Collections;

public class GridOverlay : MonoBehaviour {
	
	public GameObject plane;
	
	public bool showMain = true;
	public bool showSub = false;
	
	public int gridSizeX;
	public int gridSizeY;
	public int gridSizeZ;
	
	public float smallStep;
	public float largeStep;
	
	public float startX;
	public float startY;
	public float startZ;
	
	private float offsetY = 0;
	
	private Material lineMaterial;
	
	private Color mainColor = new Color(1f,0f,0f,1f);
	private Color subColor = new Color(0f,0.7f,0f,1f);

	private Vector3 from;
	private Vector3 to;
	public Vector3 center;

	void CreateLineMaterial() {
		
		if( !lineMaterial ) {
			lineMaterial = new Material( "Shader \"Lines/Colored Blended\" {" +
			                            "SubShader { Pass { " +
			                            "    Blend SrcAlpha OneMinusSrcAlpha " +
			                            "    ZWrite Off Cull Off Fog { Mode Off } " +
			                            "    BindChannels {" +
			                            "      Bind \"vertex\", vertex Bind \"color\", color }" +
			                            "} } }" );
			lineMaterial.hideFlags = HideFlags.HideAndDontSave;
			lineMaterial.shader.hideFlags = HideFlags.HideAndDontSave;}
	}

	public void Update(){
		this.from = new Vector3((int) (center.x - this.gridSizeX / 2F),
		                        (int) (center.y - this.gridSizeY / 2F),
		                        (int) (center.z - this.gridSizeZ / 2F));
		this.to = new Vector3((int) (center.x + this.gridSizeX / 2F),
		                      (int) (center.y + this.gridSizeY / 2F),
		                      (int) (center.z + this.gridSizeZ / 2F));
	}

	public void OnPostRender(){
		CreateLineMaterial();
		// set the current material
		lineMaterial.SetPass( 0 );
		GL.Begin( GL.LINES );

		if (this.showSub){
			GL.Color(this.subColor);
			
			for(float y = this.from.y; y <= this.to.y; y+= this.smallStep){
				//Draw lines on Z
				for (float x = this.from.x; x <= this.to.x; x+= this.smallStep){
					GL.Vertex3(x, y, this.from.z);
					GL.Vertex3(x, y, this.to.z);
				}
				//Draw lines on X
				for (float z = this.from.z; z <= this.to.z; z+= this.smallStep){
					GL.Vertex3(this.from.x, y, z);
					GL.Vertex3(this.to.x, y, z);
				}
			}
			
			for (float x = this.from.x; x <= this.to.x; x+= this.smallStep){
				for (float z = this.from.z; z <= this.to.z; z+= this.smallStep){
					GL.Vertex3(x, this.from.y, z);
					GL.Vertex3(x, this.to.y, z);
				}
			}
		}

		if (this.showMain){
			GL.Color(this.mainColor);
			
			for(float y = this.from.y; y <= this.to.y; y+= this.largeStep){
				//Draw lines on Z
				for (float x = this.from.x; x <= this.to.x; x+= this.largeStep){
					GL.Vertex3(x, y, this.from.z);
					GL.Vertex3(x, y, this.to.z);
				}
				//Draw lines on X
				for (float z = this.from.z; z <= this.to.z; z+= this.largeStep){
					GL.Vertex3(this.from.x, y, z);
					GL.Vertex3(this.to.x, y, z);
				}
			}
			
			for (float x = this.from.x; x <= this.to.x; x+= this.largeStep){
				for (float z = this.from.z; z <= this.to.z; z+= this.largeStep){
					GL.Vertex3(x, this.from.y, z);
					GL.Vertex3(x, this.to.y, z);
				}
			}
			
		}

		GL.End();
	}
	void OnPostRender2() {        
		CreateLineMaterial();
		// set the current material
		lineMaterial.SetPass( 0 );
		
		GL.Begin( GL.LINES );
		
		if(showSub){
			GL.Color(subColor);
			
			//Layers
			for(float j = 0; j <= gridSizeY; j += smallStep){
				//X axis lines
				for(float i = 0; i <= gridSizeZ; i += smallStep){
					GL.Vertex3( startX, j + offsetY, startZ + i);
					GL.Vertex3( startX + gridSizeX, j + offsetY, startZ + i);
				}
				
				//Z axis lines
				for(float i = 0; i <= gridSizeX; i += smallStep){
					GL.Vertex3( startX + i, j + offsetY, startZ);
					GL.Vertex3( startX + i, j + offsetY, gridSizeZ);
				}
			}
			
			//Y axis lines
			for(float i = 0; i <= gridSizeZ; i += smallStep){
				for(float k = 0; k <= gridSizeX; k += smallStep){
					GL.Vertex3( startX + k, startY + offsetY, startZ + i);
					GL.Vertex3( startX + k, gridSizeY + offsetY, startZ + i);
				}
			}
		}
		
		if(showMain){
			GL.Color(mainColor);
			
			//Layers
			for(float j = 0; j <= gridSizeY; j += largeStep){
				//X axis lines
				for(float i = 0; i <= gridSizeZ; i += largeStep){
					GL.Vertex3( startX, j + offsetY, startZ + i);
					GL.Vertex3( startX + gridSizeX, j + offsetY, startZ + i);
				}
				
				//Z axis lines
				for(float i = 0; i <= gridSizeX; i += largeStep){
					GL.Vertex3( startX + i, j + offsetY, startZ);
					GL.Vertex3( startX + i, j + offsetY, gridSizeZ);
				}
			}
			
			//Y axis lines
			for(float i = 0; i <= gridSizeZ; i += largeStep){
				for(float k = 0; k <= gridSizeX; k += largeStep){
					GL.Vertex3( startX + k, startY + offsetY, startZ + i);
					GL.Vertex3( startX + k, gridSizeY + offsetY, startZ + i);
				}
			}
		}
		
		
		GL.End();
	}
}