using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class LevelEditor2LC : LevelController {
	private Mode currentMode = Mode.EDIT;
	public LECameraController leCamera; //LevelEditorCamera
	private CustomLevel customLevel = new CustomLevel();

	public float scrollWheelSensitivity = 1F;

	private List<GameObject> selectedObjects = new List<GameObject>();

	private AxisHandles axisHandles;
	
	//UI elements
	private Text prefabLabel = null;

	private InputField scaleXInput = null;
	private InputField scaleYInput = null;
	private InputField scaleZInput = null;

	private InputField positionXInput = null;
	private InputField positionYInput = null;
	private InputField positionZInput = null;

	private InputField rotationXInput = null;
	private InputField rotationYInput = null;
	private InputField rotationZInput = null;

	//Editors
	private GameObject positionEditor;
	private GameObject rotationEditor;
	private GameObject scaleEditor;
	private GameObject textureEditor;
	private GameObject scriptEditor;
	private GameObject lightEditor;
	
	private Toggle collidesToggle = null;

	private SimpleJSON.JSONClass levelInfo = new SimpleJSON.JSONClass();
    //Editor Fields

    //Navmesh Fields
	private GameObject navmeshTriResource;
	private GameObject navmeshVertexResource;
	private List<GameObject> navmeshItems = new List<GameObject>();
	// Use this for initialization
	void Start () {
		this.leCamera = Camera.main.gameObject.AddComponent<LECameraController>();

		this.getInputThings();

		this.axisHandles = GameObject.Instantiate(Resources.Load<GameObject>("AxisHandle")).GetComponent<AxisHandles>();

		this.navmeshTriResource = Resources.Load<GameObject>("NavmeshTri");
		this.navmeshVertexResource = Resources.Load<GameObject>("NavmeshVertex");
	}
	
	// Update is called once per frame
	void Update () {
		if (this.currentMode == Mode.EDIT){
            this.handleEditMode();
		}else if (this.currentMode == Mode.NAVMESH){
            this.handleNavmeshMode();
		}
	}

    private void handleEditMode() {
        if(Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject() && !this.axisHandles.checkHit()) {
            GameObject hit = this.getClickedGameObject();

            if(hit != null) {
                if(Input.GetKeyDown(KeyCode.LeftShift)) {
                    if(this.selectedObjects.Contains(hit)) {
                        this.unselectObject(hit);
                    } else {
                        this.selectObject(hit, true);
                    }
                } else {
                    this.selectObject(hit, false);
                }
            } else {
                this.clearSelectedObjects();
            }

            this.toggleEditors();
        }

        if(Input.GetKeyDown(KeyCode.Delete)) {
            foreach(GameObject go in this.selectedObjects) {
                GameObject.Destroy(go);
            }
            this.selectedObjects.Clear();
            this.axisHandles.setTargets(this.selectedObjects);
        }

        this.updateDisplayValues();
    }
    private void handleNavmeshMode() {
        if(Input.GetKeyDown(KeyCode.V) && !EventSystem.current.IsPointerOverGameObject()) {//Vertex creation
            Vector3 point = this.getPointForTriFromMouse();

            if(point != Vector3.zero) {
                //Instantiate NavmeshVertexPrefab
                GameObject vertex = GameObject.Instantiate(this.navmeshVertexResource);
                vertex.transform.position = point;
                //Set the selected object to the created vertex
                this.selectObject(vertex, true);
                while(this.selectedObjects.Count > 3) {
                    this.unselectAt(0);
                }
            }
        }

        if(Input.GetKeyDown(KeyCode.C) && !EventSystem.current.IsPointerOverGameObject()) {//Tri creation
            //If there are two Vertex selected create new Tri at the mouse position
            if(this.selectedObjects.Count == 2) {
                Vector3 point = this.getPointForTriFromMouse();

                if(point != Vector3.zero) {
                    //Instantiate the NavmeshVertexPrefab
                    NavmeshVertexScript vertex = GameObject.Instantiate(this.navmeshVertexResource).GetComponent<NavmeshVertexScript>();
                    NavmeshTriScript tri;
                    vertex.transform.position = point;
                    this.selectObject(vertex.gameObject, true);
                    tri = GameObject.Instantiate(this.navmeshTriResource).GetComponent<NavmeshTriScript>();//Instantiate the Tri
                    //Set the tris's vertices and vice-versa
                    foreach(GameObject go in this.selectedObjects) {
                        NavmeshVertexScript ver = go.GetComponent<NavmeshVertexScript>();
                        tri.vertices.Add(ver);
                        ver.tris.Add(tri);
                    }
                    //Update the Tri's Mesh
                    tri.updadeMesh();
                    this.unselectAt(0);
                }
            } else if(this.selectedObjects.Count == 3) {
                GameObject tri = GameObject.Instantiate(this.navmeshTriResource);//Create the NavmeshTriPrefab
                NavmeshTriScript nts = tri.GetComponent<NavmeshTriScript>();
                NavmeshVertexScript nvs = this.selectedObjects[0].GetComponent<NavmeshVertexScript>();

                nvs.tris.Add(nts);
                nts.vertices.Add(nvs);

                nvs = this.selectedObjects[1].GetComponent<NavmeshVertexScript>();
                nvs.tris.Add(nts);
                nts.vertices.Add(nvs);

                nvs = this.selectedObjects[2].GetComponent<NavmeshVertexScript>();
                nvs.tris.Add(nts);
                nts.vertices.Add(nvs);

                nts.updadeMesh();
                this.unselectAt(0);
            } else {
                //Just in case shit happens
                DebugMenu.log("LevelEditor2LC::Update - Selected more objects than allowed");
                this.clearSelectedObjects();
            }
        }

        if(Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject() && !this.axisHandles.checkHit()) {
            GameObject go = this.getClickedGameObject();

            if(go == null) {
                this.clearSelectedObjects();
                return;
            }
            //Can only click the vertexes, so whathever about the Tris
            if(go.GetComponent<NavmeshVertexScript>() != null) {//if it's a vertex
                this.selectObject(go, Input.GetKey(KeyCode.LeftShift));
            } else if(go.GetComponent<NavmeshTriScript>() != null) {

            }
        }
        if(Input.GetKeyDown(KeyCode.Delete) && !EventSystem.current.IsPointerOverGameObject()) {
            this.doNavmeshDeletion();
        }
    }

	private void doNavmeshDeletion(){
		GameObject go = this.getClickedGameObject();
		//The tris shouldn't be selectable
		NavmeshVertexScript ver = go.GetComponent<NavmeshVertexScript>();
		this.unselectObject(go);
		if (ver != null){
			ver.delete();
		}
	}
    #region Select object region
    private void selectObject(GameObject go, bool add){
		if (!add){
			foreach(GameObject goInList in this.selectedObjects){
                this.setAlpha(goInList, 1F);
			}
			this.selectedObjects.Clear();
		}

        this.setAlpha(go, 0.5F);
		this.selectedObjects.Add(go);
		this.axisHandles.setTargets(this.selectedObjects);
	}

	private void unselectObject(GameObject go){
		this.selectedObjects.Remove(go);
        this.setAlpha(go, 1F);
		this.axisHandles.setTargets(this.selectedObjects);
	}

	private void unselectAt(int index){
		GameObject go = this.selectedObjects[index];
        this.selectedObjects.RemoveAt(index);
        this.setAlpha(go, 1F);
		this.axisHandles.setTargets(this.selectedObjects);
	}

	private void clearSelectedObjects(){
		foreach(GameObject go in this.selectedObjects){
            this.setAlpha(go, 1F);
		}
		this.selectedObjects.Clear();
		this.axisHandles.setTargets(this.selectedObjects);
	}
    
    private void setAlpha(GameObject go, float alpha) {
        Renderer renderer = go.transform.root.GetComponent<Renderer>();

        if(renderer != null) {
            Color col = renderer.material.color;
            col.a = alpha;
            renderer.material.color = col;
        }
    }
    #endregion
	private Vector3 getPointForTriFromMouse(){
		Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		
		if (Physics.Raycast(r, out hit, 1000F)){
			Vector3 point = hit.point;
			point.y += 0.1f;
			return point;
		}else{
			return Vector3.zero;
		}
	}

	private GameObject getClickedGameObject(){
		Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;

		Debug.DrawRay(r.origin, r.direction, Color.red, 10F);

		if (Physics.Raycast(r, out hit, 1000F)){
			return hit.transform.root.gameObject;
		}
			
		return null;
	}

	private void toggleEditors(){
		switch(this.currentMode){
		case Mode.NAVMESH:
			this.positionEditor.SetActive(false);
			this.rotationEditor.SetActive(false);
			this.scaleEditor.SetActive(false);
			this.textureEditor.SetActive(false);
			this.lightEditor.SetActive(false);
			this.scriptEditor.SetActive(false);
			break;
		case Mode.EDIT:
			if (this.selectedObjects.Count > 1){
				this.positionEditor.SetActive(false);
				this.rotationEditor.SetActive(false);
				this.scaleEditor.SetActive(false);
				this.textureEditor.SetActive(false);
				this.lightEditor.SetActive(false);
				this.scriptEditor.SetActive(false);
			}else if (this.selectedObjects.Count == 1){
				PlacedObjectInfo info = this.selectedObjects[0].GetComponent<PlacedObjectInfo>();
				this.positionEditor.SetActive(true);
				this.rotationEditor.SetActive(true);
				this.scaleEditor.SetActive(true);

				if (info.isPrimitive || info.isPrefab){
					this.textureEditor.SetActive(true);
					this.lightEditor.SetActive(false);
				}else{
					this.textureEditor.SetActive(false);
					this.lightEditor.SetActive(true);
				}
			}
			break;
		}
	}

	#region GUI edit hooks.
	public void updateScale(){
		try{
			float sx = float.Parse(this.scaleXInput.text);
			float sy = float.Parse(this.scaleYInput.text);
			float sz = float.Parse(this.scaleZInput.text);
			this.selectedObjects[0].transform.localScale = new Vector3(sx, sy, sz);
		}catch(System.Exception){}
	}

	public void updatePosition(){
		try{
			float x = float.Parse(this.positionXInput.text);
			float y = float.Parse(this.positionYInput.text);
			float z = float.Parse(this.positionZInput.text);
			this.selectedObjects[0].transform.position = new Vector3(x, y, z);
		}catch(System.Exception){}
	}

	public void updateRotation(){
		try{
			float x = float.Parse(this.rotationXInput.text);
			float y = float.Parse(this.rotationYInput.text);
			float z = float.Parse(this.rotationZInput.text);
			this.selectedObjects[0].transform.rotation = Quaternion.Euler(x, y, z);
		}catch(System.Exception){}
	}
	#endregion

	#region Change object methods
	public void createObject(PlaceableObject po){		
		GameObject spawned = (GameObject) GameObject.Instantiate(Resources.Load<GameObject>(po.getPath()));
		PlacedObjectInfo info = spawned.AddComponent<PlacedObjectInfo>();
        Vector3 pos = this.getPointFromCamera();
		//@TODO replace Resources.Load with a cached version.
		//@TODO Save all the layers for each individual part of the prefab in a treemap
		this.updateDisplayValues();

		info.isLight = false;
		info.isPrefab = true;
		info.isPrimitive = false;

		info.placeableObject = po;
		info.collides = this.collidesToggle.isOn;

        spawned.transform.root.position = pos;

        this.selectObject(spawned.transform.root.gameObject, false);
		this.updateDisplayValues();
		this.toggleEditors();
	}
	
	public void createObject(LightType lightType){
		GameObject spawned = GameObject.Instantiate(Resources.Load<GameObject>(PlaceableObject.LIGHT_GIZMO.getPath()));
		PlacedObjectInfo info = spawned.AddComponent<PlacedObjectInfo>();
		Light light = spawned.AddComponent<Light>();
        Vector3 pos = this.getPointFromCamera();

		this.selectedObjects.Clear();
		this.selectedObjects.Add(spawned);

		info.isLight = true;
		info.isPrefab = false;
		info.isPrimitive = false;

		info.lightType = lightType;
		light.type = lightType;

        spawned.transform.position = pos;

        this.selectObject(spawned, false);
		this.updateDisplayValues();
		this.toggleEditors();
	}
	
	
	public void createObject(PrimitiveType type){
		GameObject spawned = GameObject.CreatePrimitive(type);
		PlacedObjectInfo info = spawned.AddComponent<PlacedObjectInfo>();
        Vector3 pos = this.getPointFromCamera();

		info.isLight = false;
		info.isPrefab = false;
		info.isPrimitive = true;
		info.primitiveType = type;
		info.collides = this.collidesToggle.isOn;

        spawned.transform.position = pos;

        this.selectObject(spawned, false);
		this.updateDisplayValues();
		this.toggleEditors();
	}

    private Vector3 getPointFromCamera() {
        RaycastHit hit;

        if(Physics.Raycast(new Ray(this.leCamera.transform.position, this.leCamera.transform.forward), out hit, 1000F)) {
            return hit.point;
        } else {
            return Vector3.zero;
        }
    }
	#endregion

	private void updateDisplayValues(){
		if (this.selectedObjects.Count == 0){
			return;
		}

		Vector3 scale = this.selectedObjects[0].transform.localScale;
		Vector3 pos = this.selectedObjects[0].transform.position;
		Vector3 rot = this.selectedObjects[0].transform.localEulerAngles;

		this.prefabLabel.text = "Prefab: " + this.selectedObjects[0];

		this.scaleXInput.text = "" + scale.x;
		this.scaleYInput.text = "" + scale.y;
		this.scaleZInput.text = "" + scale.z;

		this.positionXInput.text = "" + pos.x;
		this.positionYInput.text = "" + pos.y;
		this.positionZInput.text = "" + pos.z;

		this.rotationXInput.text = "" + rot.x;
		this.rotationYInput.text = "" + rot.y;
		this.rotationZInput.text = "" + rot.z;
	}
	
	public override void tick () {
		//All the updates should be done in Update()
	}
	
	public override void onEscPressed(){
		this.selectedObjects.Clear();
	}
	
	public override void end() {
		//@TODO Save
		this.updateCustomLevel();
		LevelLoader.saveLevel("Exit_Save", this.customLevel);
	}

    public CustomLevel getCustomLevel() {
        this.updateCustomLevel();
        return this.customLevel;
    }

    private SimpleJSON.JSONClass createNavmeshClass() {
        SimpleJSON.JSONClass navmesh = new SimpleJSON.JSONClass();
        LinkedList<NavmeshVertexScript> vertices = new LinkedList<NavmeshVertexScript>();
        LinkedList<NavmeshTriScript> tris = new LinkedList<NavmeshTriScript>();
        int i = 0;

        foreach(GameObject go in this.navmeshItems) {
            if(go.GetComponent<NavmeshVertexScript>() != null) {
                vertices.AddFirst(go.GetComponent<NavmeshVertexScript>());
            } else {
                tris.AddFirst(go.GetComponent<NavmeshTriScript>());
            }
        }

        foreach(NavmeshVertexScript vertex in vertices) {
            Vector3 pos = vertex.transform.position;
            vertex.index = i;
            navmesh["Vertices"][i]["X"].AsFloat = pos.x;
            navmesh["Vertices"][i]["Y"].AsFloat = pos.y;
            navmesh["Vertices"][i]["Z"].AsFloat = pos.z;
            i++;
        }

        i = 0;

        foreach(NavmeshTriScript tri in tris) {
            //Vertex1
            //Vertex2
            //Vertex3
            //Cost
            //MaxSize
            //NormalX
            //NormalY
            //NormalZ
            Vector3 normal = tri.getNormal();

            navmesh["Tris"][i]["Vertex1"].AsInt = tri.vertices[0].index;
            navmesh["Tris"][i]["Vertex2"].AsInt = tri.vertices[1].index;
            navmesh["Tris"][i]["Vertex3"].AsInt = tri.vertices[2].index;

            navmesh["Tris"][i]["Cost"].AsFloat = tri.cost;
            navmesh["Tris"][i]["MaxSize"].AsFloat = tri.maxSize;

            navmesh["Tris"][i]["NormalX"].AsFloat = normal.x;
            navmesh["Tris"][i]["NormalY"].AsFloat = normal.y;
            navmesh["Tris"][i]["NormalZ"].AsFloat = normal.z;

            i++;
        }

        return navmesh;
    }

	public override void reload () {
        DebugMenu.log("Level editor crashed.");

        foreach(PlacedObjectInfo info in GameObject.FindObjectsOfType<PlacedObjectInfo>()) {
            GameObject.Destroy(info.transform.gameObject);
        }
	}
	
	public override void onWindowResize () {
		//nothing
	}

	public Mode getCurrentMode(){
		return this.currentMode;
	}

	public void setMode(Mode mode){
		switch(mode){
			case Mode.EDIT:
				if (currentMode == Mode.NAVMESH){
					NavmeshTriScript[] tris = GameObject.FindObjectsOfType<NavmeshTriScript>();
					NavmeshVertexScript[] vertices = GameObject.FindObjectsOfType<NavmeshVertexScript>();
					this.navmeshItems = new List<GameObject>(tris.Length + vertices.Length);
                    this.clearSelectedObjects();
					
					foreach(NavmeshTriScript tri in tris){
						tri.gameObject.SetActive(false);
						navmeshItems.Add(tri.gameObject);
					}
					foreach(NavmeshVertexScript vertex in vertices){
						vertex.gameObject.SetActive(false);
						navmeshItems.Add(vertex.gameObject);
					}

					this.currentMode = Mode.EDIT;
				}
				break;
			case Mode.NAVMESH:
				if (currentMode == Mode.EDIT){
                    this.clearSelectedObjects();
					if (this.navmeshItems.Count > 0){
						foreach(GameObject go in this.navmeshItems){
							go.SetActive(true);
						}
					}
					this.currentMode = Mode.NAVMESH;
				}
				break;
			default:
				this.currentMode = Mode.EDIT;
				throw new System.Exception("Unknown mode: " + mode);
		}
		this.toggleEditors();
		Debug.Log("@TODO finish LevelEditor2LC::setMode");
	}

	public void loadLevel(string name){
        this.clearSelectedObjects();
		SimpleJSON.JSONClass navmesh;
		NavmeshVertexScript[] vertices;
		NavmeshTriScript[] tris;

        foreach(GameObject go in navmeshItems) {
            GameObject.Destroy(go);
        }

        this.navmeshItems.Clear();

		foreach(PlacedObjectInfo info in GameObject.FindObjectsOfType<PlacedObjectInfo>()){
			GameObject.Destroy(info.transform.root.gameObject);
		}

		this.customLevel = LevelLoader.loadLevel(name);
		navmesh = this.customLevel.getNavmesh();
		vertices = new NavmeshVertexScript[navmesh["Vertices"].Count];
		tris = new NavmeshTriScript[navmesh["Tris"].Count];

		DebugMenu.log("Loading items.");

		foreach(LevelItem item in this.customLevel.getItems()){
			item.place(true);
		}

		DebugMenu.log("Loading navmesh");

		for(int i = 0; i < vertices.Length; i++){
			Vector3 pos = new Vector3(navmesh["Vertices"][i]["X"].AsFloat, navmesh["Vertices"][i]["Y"].AsFloat, navmesh["Vertices"][i]["Z"].AsFloat);
			vertices[i] = ((GameObject) GameObject.Instantiate(this.navmeshVertexResource, pos, Quaternion.identity)).GetComponent<NavmeshVertexScript>();
		}

		for(int i = 0; i < tris.Length; i++){
			NavmeshVertexScript v1 = vertices[navmesh["Tris"][i]["Vertex1"].AsInt];
			NavmeshVertexScript v2 = vertices[navmesh["Tris"][i]["Vertex2"].AsInt];
			NavmeshVertexScript v3 = vertices[navmesh["Tris"][i]["Vertex3"].AsInt];
			tris[i] = ((GameObject) GameObject.Instantiate(this.navmeshTriResource, Vector3.zero, Quaternion.identity)).GetComponent<NavmeshTriScript>();

			tris[i].vertices.Add(v1);
            v1.tris.Add(tris[i]);

			tris[i].vertices.Add(v2);
            v2.tris.Add(tris[i]);

			tris[i].vertices.Add(v3);
            v3.tris.Add(tris[i]);

			tris[i].cost = navmesh["Tris"][i]["Cost"].AsFloat;
			tris[i].maxSize = navmesh["Tris"][i]["MaxSize"].AsFloat;
			tris[i].updadeMesh();
		}

		Debug.Log("@TODO finish the LevelEditor2LC::loadLevel(string name)");
	}

	private void updateCustomLevel(){
        SimpleJSON.JSONClass navmesh = this.createNavmeshClass();
        PlacedObjectInfo[] placedObjects = GameObject.FindObjectsOfType<PlacedObjectInfo>();
        LevelItem[] levelItems = new LevelItem[placedObjects.Length];
        int i = 0;
        
        if(this.customLevel == null) {
            this.customLevel = new CustomLevel();
        }

        foreach(PlacedObjectInfo po in placedObjects) {
            levelItems[i] = new LevelItem(po);
            i++;
        }

        this.customLevel.setNavmesh(navmesh);
        this.customLevel.setTextures(new SimpleJSON.JSONClass());
        this.customLevel.setItems(levelItems);
        this.customLevel.setLevelInfo(this.levelInfo);
	}

	private void getInputThings(){
		this.positionEditor = GameObject.Find("ObjectCanvas/ObjectInfoPanel/PositionLabel");
		this.rotationEditor = GameObject.Find("ObjectCanvas/ObjectInfoPanel/RotationLabel");
		this.scaleEditor = GameObject.Find("ObjectCanvas/ObjectInfoPanel/ScaleLabel");
		this.textureEditor = GameObject.Find("ObjectCanvas/ObjectInfoPanel/TextureLabel");
		this.lightEditor = GameObject.Find("ObjectCanvas/ObjectInfoPanel/LightTypeLabel");
		this.scriptEditor = new GameObject();

		this.scaleXInput = GameObject.Find("ObjectCanvas/ObjectInfoPanel/ScaleLabel/InputScaleX").GetComponent<InputField>();
		this.scaleYInput = GameObject.Find("ObjectCanvas/ObjectInfoPanel/ScaleLabel/InputScaleY").GetComponent<InputField>();
		this.scaleZInput = GameObject.Find("ObjectCanvas/ObjectInfoPanel/ScaleLabel/InputScaleZ").GetComponent<InputField>();

		this.positionXInput = GameObject.Find("ObjectCanvas/ObjectInfoPanel/PositionLabel/InputPositionX").GetComponent<InputField>();
		this.positionYInput = GameObject.Find("ObjectCanvas/ObjectInfoPanel/PositionLabel/InputPositionY").GetComponent<InputField>();
		this.positionZInput = GameObject.Find("ObjectCanvas/ObjectInfoPanel/PositionLabel/InputPositionZ").GetComponent<InputField>();

		this.rotationXInput = GameObject.Find("ObjectCanvas/ObjectInfoPanel/RotationLabel/InputRotationX").GetComponent<InputField>();
		this.rotationYInput = GameObject.Find("ObjectCanvas/ObjectInfoPanel/RotationLabel/InputRotationY").GetComponent<InputField>();
		this.rotationZInput = GameObject.Find("ObjectCanvas/ObjectInfoPanel/RotationLabel/InputRotationZ").GetComponent<InputField>();

		this.collidesToggle = GameObject.Find("ObjectCanvas/ObjectInfoPanel/CollidesToggle").GetComponent<Toggle>();

		this.prefabLabel = GameObject.Find("ObjectCanvas/ObjectInfoPanel/PrefabNameText").GetComponent<Text>();
	}

	public enum Mode{
		EDIT = 1,
		NAVMESH = 2
	}
}
