﻿using UnityEngine;
using System.Collections;

public abstract class LevelController : MonoBehaviour{
	//Use Start() for initialization
	//public abstract void init(); 

	public abstract void tick();
	//Use OnDestroy()
	public abstract void end();

	public abstract void reload();

	public abstract void onEscPressed();

	public abstract void onWindowResize();
}
