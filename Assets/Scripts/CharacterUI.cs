﻿using UnityEngine;
using System.Collections;
//@TODO replace with canvas prefab, maybe add the UI script to the canvas to get the player's info
public class CharacterUI : MonoBehaviour {
	private MG left;
	private MG right;
	public Camera camera;

	public GUIStyle leftSkin;
	public GUIStyle rightSkin;
	private LayerMask playerLayer;

	//public Vector3 left3DMGHookPos;
	//public Vector3 right3DMGHookPos;

	public Vector3 mgHookPos;

	private float yScreenTargetPos;
	private float xScreenTargetPos;
	// Use this for initialization
	void Start () {
		MG[] arr = GetComponentsInChildren<MG>();
		left = arr[0];
		right = arr[1];

		playerLayer = LayerMask.NameToLayer("PlayerLayer");

		if (camera == null){
			camera = Camera.main;
		}

		xScreenTargetPos = Screen.width / 2;
		yScreenTargetPos = Screen.height / 2;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void FixedUpdate(){
		RaycastHit hit;
		
		if (Physics.Raycast(new Ray(this.camera.transform.position, Camera.main.transform.forward), out hit, 1000f, playerLayer)){
			mgHookPos = hit.point;
			//Vector3 a = camera.WorldToScreenPoint(hit.point);
		}else{
			mgHookPos = Vector3.zero;
		}
	}

	public void OnGUI(){
		this.displayCursor();
		this.displayVersion();
	}

	private void displayCursor(){
		GUI.Label(new Rect(xScreenTargetPos, yScreenTargetPos, 50, 50), "+", leftSkin);

	}
	/*
	private void displayCursorS(){
		RaycastHit hit;
		
		if (Physics.Raycast(new Ray(this.camera.transform.position, Camera.main.transform.forward), out hit, 1000f, playerLayer)){
			left3DMGHookPos = hit.point;
			Vector3 a = camera.WorldToScreenPoint(hit.point);
			GUI.Label(new Rect(a.x - 1, a.y + 1, 50, 50), "+", leftSkin);
		}else{
			left3DMGHookPos = Vector3.zero;
		}
		
		if (Physics.Raycast(new Ray(right.transform.position, Camera.main.transform.forward), out hit, 1000f, playerLayer)){
			right3DMGHookPos = hit.point;
			Vector3 a = camera.WorldToScreenPoint(hit.point);
			GUI.Label(new Rect(a.x - 1, a.y + 1, 50, 50), "+", rightSkin);
		}else{
			right3DMGHookPos = Vector3.zero;
		}

	}
	*/
	private void displayVersion(){
		Color old = GUI.contentColor;
		GUI.contentColor = Color.black;
		GUI.Label(new Rect(Screen.width - 90, 0, 100, 20), "Version 0.0.1");
		GUI.contentColor = old;
	}
}
