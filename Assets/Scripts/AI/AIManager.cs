using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Priority_Queue;
public class AIManager{
	private HeapPriorityQueue<TitanWrapper> titans = new HeapPriorityQueue<TitanWrapper>(50);
	private object titansLock = new object();

	public const int MAX_THREAT = 100;
	public const long TICK_INTERVAL = 100;//10 times a second
	public const int TITANS_PER_TICK = 20;

	private readonly GameObject titanPrefab;
	private volatile bool toRun = false;

	public AIManager (){
		titanPrefab = Resources.Load<GameObject>(ResourcePath.TITAN.getPath());
	}
	

	public void cleanup(){
		titans.Clear();
		this.toRun = false;
	}
	//Maybe just scrap this way of doing it and just tick all the titans and have them check whether they can see
	//something or not
	public void run(){
		List<TitanWrapper> tickedTitans = new List<TitanWrapper>();
        long startTick;
        long interval = 0L;

		while(this.toRun){
			if (titans.Count > 0){

				lock(this.titansLock){
                    startTick = System.DateTime.Now.Ticks;

					for(int i = 0; titans.Count > 0 && i < AIManager.TITANS_PER_TICK; i++){
						TitanWrapper wrapper = titans.Dequeue();

						try{
							int threat = Mathf.Clamp(wrapper.titan.tick(), 0, 100);
							wrapper.Priority = threat;
							tickedTitans.Add(wrapper);
						}catch(System.Exception e){
							DebugMenu.log("Titan " + wrapper.titan + " crashed.");
							wrapper.titan.destroy();
							Debug.LogException(e);
						}
					}

					foreach(TitanWrapper tw in tickedTitans){
						titans.Enqueue(tw, tw.Priority);
					}

					tickedTitans.Clear();

                    interval = (DateTime.Now.Ticks - startTick) / TimeSpan.TicksPerMillisecond;
				}
			}

            if(interval < AIManager.TICK_INTERVAL) {
                Thread.Sleep((int) ((AIManager.TICK_INTERVAL - interval)));
            }
		}
	}

	public void spawnTitanAt(Vector3 position, int priority){
        GameObject spawned = (GameObject) GameObject.Instantiate(this.titanPrefab, position, Quaternion.identity);
        TitanWrapper wrapper = new TitanWrapper(spawned.GetComponent<TestAI>());
	}

	private class TitanWrapper : PriorityQueueNode{
		public readonly ITitanAI titan;

		public TitanWrapper (ITitanAI titan){
			this.titan = titan;
		}
	}
}

