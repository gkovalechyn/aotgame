﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.AI {
    class BodyPart {
        public static BodyPart HEAD = new BodyPart("/Spine/Neck/Head");
        public static BodyPart NECK = new BodyPart("/Spine/Neck");
        public static BodyPart SPINE = new BodyPart("/Spine");

        public static BodyPart LEFT_HAND = new BodyPart("/Spine/LeftUpperArm/LeftLowerArm/LeftHand");
        public static BodyPart LEFT_LOWER_ARM = new BodyPart("/Spine/LeftUpperArm/LeftLowerArm");
        public static BodyPart LEFT_UPPER_ARM = new BodyPart("/Spine/LeftUpperArm");

        public static BodyPart RIGHT_HAND = new BodyPart("/Spine/RightUpperArm/RightLowerArm/RightHand");
        public static BodyPart RIGHT_LOWER_ARM = new BodyPart("/Spine/RightUpperArm/RightLowerArm");
        public static BodyPart RIGHT_UPPER_ARM = new BodyPart("/Spine/RightUpperArm");

        public static BodyPart LEFT_FOOT = new BodyPart("Spine/LeftUpperLeg/LeftLowerLeg/LeftFoot");
        public static BodyPart LEFT_LOWER_LEG = new BodyPart("Spine/LeftUpperLeg/LeftLowerLeg");
        public static BodyPart LEFT_UPPER_LEG = new BodyPart("Spine/LeftUpperLeg");

        public static BodyPart RIGHT_FOOT = new BodyPart("Spine/RightUpperLeg/RightLowerLeg/RightFoot");
        public static BodyPart RIGHT_LOWER_LEG = new BodyPart("Spine/RightUpperLeg/RightLowerLeg");
        public static BodyPart RIGHT_UPPER_LEG = new BodyPart("Spine/RightUpperLeg");

        private readonly string partPath;
        private BodyPart(string path) {
            partPath = path;
        }

        public string getPartPath() {
            return this.partPath;
        }
    }
}
