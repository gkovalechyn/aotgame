using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class TitanTarget : MonoBehaviour{
	public static List<TitanTarget> currentInLevel = new List<TitanTarget>();
	public Rigidbody rigidBody;
	// Use this for initialization
	void Start (){
		this.rigidBody = this.GetComponent<Rigidbody>();

		TitanTarget.currentInLevel.Add(this);
	}

	public void OnDestroy(){
		TitanTarget.currentInLevel.Remove(this);
	}
}

