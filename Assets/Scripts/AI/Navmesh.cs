using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Priority_Queue;

public class Navmesh{
	private List<Tri> navmesh;
	private Vector3 regionMapOffset;
	private Vector3 navmeshMaxPoint;
	private List<Tri>[][] regionMap;
	private List<Vector3> vertexes;

	public const float REGION_SIZE = 10F;

	public Navmesh(SimpleJSON.JSONClass navClass){
		int vertexCount = navClass["Vertices"].Count;
		int triCout = navClass["Tris"].Count;
		List<Vector3> vertexes = new List<Vector3>(vertexCount);
		List<Tri> tris = new List<Tri>(vertexCount);
		List<Tri>[] vertexTriMap = new List<Tri>[vertexCount];
		Vector3 maxPoint = Vector3.zero;
		Vector3 minPoint = Vector3.zero;

		for(int i = 0; i < vertexCount; i++){
			float x = navClass["Vertices"][i]["X"].AsFloat;
			float y = navClass["Vertices"][i]["Y"].AsFloat;
			float z = navClass["Vertices"][i]["Z"].AsFloat;
			Vector3 vec = new Vector3(x, y, z) ;
			vertexes.Add(vec);
			vertexTriMap[i] = new List<Tri>(8);

			if (minPoint == Vector3.zero){
				minPoint = vec;
			}else{
				minPoint.x = Mathf.Min(minPoint.x, vec.x);
				minPoint.z = Mathf.Min(minPoint.z, vec.z);
			}

			maxPoint.x = Mathf.Max(maxPoint.x, vec.x);
			maxPoint.z = Mathf.Max(maxPoint.z, vec.z);
		}

		this.regionMapOffset = minPoint;
		this.navmeshMaxPoint = maxPoint;
		//@TODO Tri loading

		for(int i = 0; i < triCout; i++){
			SimpleJSON.JSONClass currentTri = (SimpleJSON.JSONClass) navClass["Tris"][i];
			Tri tri = new Tri();
			Vector3 p1 = this.vertexes[currentTri["Vertex1"].AsInt];
			Vector3 p2 = this.vertexes[currentTri["Vertex2"].AsInt];
			Vector3 p3 = this.vertexes[currentTri["Vertex3"].AsInt];

			tri.Cost = currentTri["Cost"].AsFloat;
			tri.MaxSize = currentTri["MaxSize"].AsFloat;

			tri.P1 = p1;
			tri.P1Index = currentTri["Vertex1"].AsInt;

			tri.P2 = p2;
			tri.P2Index = currentTri["Vertex2"].AsInt;

			tri.P3 = p3;
			tri.P3Index = currentTri["Vertex3"].AsInt;

			tri.Normal = new Vector3(currentTri["NormalX"].AsFloat, currentTri["NormalY"].AsFloat, currentTri["NormalZ"].AsFloat);

			vertexTriMap[currentTri["Vertex1"].AsInt].Add(tri);
			vertexTriMap[currentTri["Vertex2"].AsInt].Add(tri);
			vertexTriMap[currentTri["Vertex3"].AsInt].Add(tri);

			this.computeRegionsFor(tri);
		}

		foreach(List<Tri> triList in vertexTriMap){
			foreach(Tri tmpTri in triList){
				List<Tri> neighbours = new List<Tri>(triList);

				neighbours.Remove(tmpTri);
				tmpTri.ConnectedTo = neighbours;
			}
		}
	}

	//http://www.redblobgames.com/pathfinding/a-star/introduction.html
	public LinkedList<Tri> getPath(Vector3 from, Vector3 to, float entityWidth, float entityHeight){
		HeapPriorityQueue<Tri> frontier;
		Dictionary<Tri, bool> visitedMap = new Dictionary<Tri, bool>();
		Dictionary<Tri, float> cost = new Dictionary<Tri, float>();
		Dictionary<Tri, Tri> cameFrom = new Dictionary<Tri, Tri>();
		Dictionary<Tri, int> triPass = new Dictionary<Tri, int>();
		Tri triFrom;
		Tri triTo;
		int queueSize = 0;
		float widthSquared = entityWidth * entityWidth;
		//Estimate the queue size.
		//Calculate the number of regions between the two points and assume a number of tris per region
		//Assume that there is a straight path to the destination and use the manhattan distance
		//int intersectingRegionCount = Mathf.CeilToInt(Mathf.Abs(to.x - from.x) + Mathf.Abs(to.z - from.z) / Navmesh.REGION_SIZE);
		//The most of tris that will be on the queue at one time will be the amount of tris
		queueSize = this.navmesh.Count;

		triFrom = this.getTriAt(from);
		if (triFrom == null){
			return null;
		}

		triTo = this.getTriAt(to);
		if (triTo == null){
			return null;
		}

		if (triFrom.Equals(triTo)){
			LinkedList<Tri> res = new LinkedList<Tri>();
			res.AddFirst(triFrom);
			return res;
		}

		frontier = new HeapPriorityQueue<Tri>(queueSize);
		frontier.Enqueue(triFrom, 0);
		cameFrom[triFrom] = null;
		cost[triFrom] = 0;
		triPass[triFrom] = 3;

		while(frontier.Count != 0){
			Tri current = frontier.Dequeue();

			if (current == triTo){
				return this.generateList(cameFrom, current);
			}

			foreach(Tri neighbour in current.ConnectedTo){
				Vector3 edge = current.getEdgeWith(neighbour);
				triPass[neighbour] = triPass.ContainsKey(neighbour) ? triPass[neighbour] + 1 : 1;

				if (edge != Vector3.zero && edge.sqrMagnitude >= widthSquared && !cost.ContainsKey(neighbour)){
					if (neighbour.Equals(triTo)){
						cameFrom[neighbour] = current;
						return this.generateList(cameFrom, neighbour);
					}

					if (entityHeight < neighbour.MaxSize){
						float newCost =  cost[current] + neighbour.Cost;

						if (!cost.ContainsKey(neighbour)){//Don't examine the same tri twice
							cost[neighbour] = newCost;
							frontier.Enqueue(neighbour, newCost + heuristic(current, neighbour));
							cameFrom[neighbour] = current;
						}
					}else{
						cost[neighbour] = -1;
					}
				}else if (triPass[neighbour] >= 3){
					cost[neighbour] = -1;//Just so that it isn't checked later
				}
			}
		}

		return null;
	}

	private LinkedList<Tri> generateList(Dictionary<Tri, Tri> cameFrom, Tri lastTri){
		LinkedList<Tri> path = new LinkedList<Tri>();//This is going to be backwards
		Tri previous = null;
		path.AddLast(lastTri);

		cameFrom.TryGetValue(lastTri, out previous);

		while(previous != null){
			path.AddFirst(previous);
			cameFrom.TryGetValue(previous, out previous);
		}

		return path;
	}

	private float heuristic(Tri current, Tri next){
		return Mathf.Abs(current.Center.x - next.Center.x) + Mathf.Abs(current.Center.z - next.Center.z);
	}
	
	public List<Tri> getTrisInRegion(Vector3 point){
		int x;
		int z;
		this.getRegionAt(point, out x, out z);
		return this.getTrisInRegion(x, z);
	}

	public List<Tri> getTrisInRegion(int x, int z){
		if (x < 0 || z < 0 || x >= this.regionMap.Length || z >= this.regionMap[x].Length){
			return null;//Maybe return an empty list?
		}else{
			return this.regionMap[x][z];
		}
	}

	public int getVertexCount(){
		return this.vertexes.Count;
	}

	public List<Vector3> getVertexes(){
		return new List<Vector3>(this.vertexes);
	}

	public int getTriCount(){
		return this.navmesh.Count;
	}

	public List<Tri> getTris(){
		return new List<Tri>(this.navmesh);
	}

	private void computeRegionsFor(Tri tri){
		Debug.Log("@TODO finish Navmesh::computeRegionsFor");
		List<int> regionCoordinates = new List<int>(8);

	}

	public Tri getTriAt(Vector3 position){
		List<Tri> tris = this.getTrisInRegion(position);

		if (tris != null){
			foreach(Tri tri in tris){
				if (tri.isPointInside(position)){
					//For now just assume 1 Y level;
					return tri;
				}
			}

			return null;
		}else{
			return null;
		}
	}

	public void getRegionAt(Vector3 pos, out int x, out int z){
		Vector3 localPoint = pos + this.regionMapOffset;
		x = Mathf.RoundToInt(localPoint.x / REGION_SIZE);
		z = Mathf.RoundToInt(localPoint.z / REGION_SIZE);

		if (x >= this.regionMap.Length || z >= this.regionMap[x].Length){
			x = -1;
			z = -1;
		}
	}

	public class Tri : PriorityQueueNode{
		private Vector3 p1;
		private int p1Index;

		private Vector3 p2;
		private int p2Index;

		private Vector3 p3;
		private int p3Index;

		private Vector3 center;
		private Vector3 normal;

		private int[] insideRegions;

		private float cost;
		private float maxSize;
		private bool walkable;

		private IList<Tri> connectedTo = new List<Tri>(3);

		private int hashCode;
		

		//https://stackoverflow.com/questions/13300904/determine-whether-point-lies-inside-triangle
		//Swapped Y with Z
		public bool isPointInside(Vector3 p){
			float alpha = ((p2.z - p3.z)*(p.x - p3.x) + (p3.x - p2.x)*(p.z - p3.z)) /
				((p2.z - p3.z)*(p1.x - p3.x) + (p3.x - p2.x)*(p1.z - p3.z));
			float beta = ((p3.z - p1.z)*(p.x - p3.x) + (p1.x - p3.x)*(p.z - p3.z)) /
				((p2.z - p3.z)*(p1.x - p3.x) + (p3.x - p2.x)*(p1.z - p3.z));
			float gamma = 1.0f - alpha - beta;

			return gamma > 0 && alpha > 0 && beta > 0;
		}

		public Vector3 getEdgeWith(Tri other){
			if (this.connectedTo.Contains(other)){
				Debug.Log("@TODO Navmesh::Tri::getEdgeWith(Tri other)");
				return Vector3.zero;
			}else{
				return Vector3.zero;
			}
		}
#region Properties	
		public Vector3 P1 {
			get {
				return this.p1;
			}
			internal set {
				p1 = value;
				this.updateCenterPoint();
				this.updateHashCode();
			}
		}

		public Vector3 P2 {
			get {
				return this.p2;
			}
			internal set {
				p2 = value;
				this.updateCenterPoint();
				this.updateHashCode();
			}
		}

		public Vector3 P3 {
			get {
				return this.p3;
			}
			internal set {
				p3 = value;
				this.updateCenterPoint();
				this.updateHashCode();
			}
		}

		public Vector3 Center {
			get {
				return this.center;
			}
		}

		public Vector3 Normal {
			get {
				return this.normal;
			}
			set {
				normal = value;
			}
		}

		public int[] InsideRegions {
			get {
				return this.insideRegions;
			}
			internal set {
				insideRegions = value;
			}
		}

		public float Cost {
			get {
				return this.cost;
			}
			set {
				cost = value;
				this.updateHashCode();
			}
		}

		public float MaxSize {
			get {
				return this.maxSize;
			}
			set {
				maxSize = value;
				this.updateHashCode();
			}
		}

		public IList<Tri> ConnectedTo {
			get {
				return this.connectedTo;
			}
			internal set {
				connectedTo = value;
			}
		}

		public bool Walkable {
			get {
				return this.walkable;
			}
			set {
				walkable = value;
			}
		}

		public int P1Index {
			get {
				return this.p1Index;
			}
			internal set {
				p1Index = value;
			}
		}

		public int P2Index {
			get {
				return this.p2Index;
			}
			internal set {
				p2Index = value;
			}
		}

		public int P3Index {
			get {
				return this.p3Index;
			}
			internal set {
				p3Index = value;
			}
		}
#endregion

		private void updateCenterPoint(){
			this.center.x = (p1.x + p2.x + p3.x) / 3F;
			this.center.y = (p1.y + p2.y + p3.y) / 3F;
			this.center.z = (p1.z + p2.z + p3.z) / 3F;
		}

		private void updateHashCode(){
			int hashCode = ((int)(p1.x + p2.z) << 3) + ((int)(p1.y + p2.y) << 5) + ((int)(p1.z + p2.x) << 7);
			hashCode *= (int) (this.maxSize * this.cost);
			hashCode *= 113;
			this.hashCode = hashCode;
		}

		public override bool Equals (object obj) {
			if (!(obj is Tri)){
				return false;
			}
			Tri tri = (Tri) obj;
			return tri.cost.Equals(this.cost) && 
				tri.maxSize.Equals(this.maxSize) && 
					tri.p1.Equals(this.p1) && 
					tri.p2.Equals(this.p2) && 
					tri.p3.Equals(this.p3);
		}
		public override int GetHashCode () {
			return this.hashCode;
		}
	}
}

