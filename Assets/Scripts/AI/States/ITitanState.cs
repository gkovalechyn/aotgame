﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.AI.States {
    interface ITitanState {

        void update();

        void fixedUpdate();

        void lateUpdate();

        void tick();
    }
}
