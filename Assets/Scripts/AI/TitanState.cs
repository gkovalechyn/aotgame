public class TitanState{
	public static TitanState IDLE = new TitanState();
	public static TitanState WALKING = new TitanState();
	public static TitanState GRABBING = new TitanState();

	public TitanState[] values = {
		IDLE,
		WALKING,
		GRABBING
	};

	private TitanState(){

	}
}

