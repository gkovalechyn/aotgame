using UnityEngine;
using System.Collections;
/// <summary>
/// Simple test AI to test the dynamic animations for grabbing and the head follow thing. Should never be used
/// ingame, the ingame AI should follow the navmesh or use the premade UI pathing.
/// Characteristics for this script:
/// -- The titan will walk to the player in a straight line;
/// -- When the player gets in the spot for the grab the titan will try to grab the player.
/// -- If the player leaves the "sense distance" the titan will go into idle mode.
/// -- Does not care about line of sight
/// </summary>
public class TestAI : MonoBehaviour, ITitanAI{
	#region Body parts Fields
	private GameObject head;

	private GameObject leftArmUpper;
	private GameObject leftArmLower;
	private GameObject leftHand;

	private GameObject rightArmUpper;
	private GameObject rightArmLower;
	private GameObject rightHand;
	#endregion

	#region Basic AI fields
	private TitanTarget currentTarget;
	private TitanState state = TitanState.IDLE;

	public float senseDistance = 100F;
	public float grabDistance = 20F;

	private AnimationClip grabAnimation = null;
	#endregion

	#region Walking stuff Fields
	public float headRotationSpeed = 0.4F;
	public float acceleration = 0.2F;
	public float walkSpeed = 10F;
	#endregion

	private Rigidbody rigidBody;

	public void Start (){
		//@TODO Get components
		this.rigidBody = this.transform.GetComponent<Rigidbody>();
	}
	
	public void Update (){
		//@TODO Change the way the states are handled
		//@TODO AI in a new thread.
		if (this.state == TitanState.IDLE){
			TitanTarget target = null;
			float distance = 0f;

			foreach(TitanTarget tt in TitanTarget.currentInLevel){
				if (target == null){
					target = tt;
					distance = (tt.transform.position - this.transform.position).sqrMagnitude;
				}else{
					//If the distance to the new target is smaller than the old one that one is the new target.
					float distanceToTarget = (tt.transform.position - this.transform.position).sqrMagnitude;

					if (distanceToTarget < this.senseDistance * this.senseDistance && distance > distanceToTarget){
						target = tt;
						distance = distanceToTarget;
					}
				}
			}
			//Didn't have a target and now has one
			if (target != null){
				this.currentTarget = target;
				this.state = TitanState.WALKING;
			}else{
				this.rigidBody.velocity += this.transform.forward * this.acceleration;
			}
		}else if (this.state == TitanState.WALKING){
			Vector3 toTarget = this.currentTarget.transform.position - this.transform.position;
			Vector3 targetFuturePosition = this.currentTarget.transform.position + this.currentTarget.rigidBody.velocity;
			if (toTarget.sqrMagnitude > this.senseDistance * this.senseDistance){
				this.currentTarget = null;
				this.state = TitanState.IDLE;
				return;
			}

			//Rotate towards the target
			Quaternion.Lerp(this.transform.rotation, Quaternion.LookRotation(toTarget, Vector3.up), 0.2F);
			//Rotate the head
			if (Vector3.Angle(this.transform.forward, toTarget) < 60F){
				this.head.transform.rotation = Quaternion.Lerp(this.head.transform.rotation, Quaternion.LookRotation(toTarget), this.headRotationSpeed);
			}else{
				this.head.transform.rotation = Quaternion.Lerp(this.head.transform.rotation, Quaternion.LookRotation(this.head.transform.forward), this.headRotationSpeed);
			}
			//accelerate
			this.rigidBody.velocity = Vector3.Lerp(this.rigidBody.velocity, this.transform.forward * this.walkSpeed, this.acceleration);

			//Check for the grab
			if ((targetFuturePosition - this.transform.position).sqrMagnitude < this.grabDistance * this.grabDistance){
				if (Vector3.Angle(this.transform.forward, targetFuturePosition) < 80F){
					this.grabAnimation = this.createGrabFor(this.currentTarget.transform.position);
					this.state = TitanState.GRABBING;
				}
			}
		}else if (this.state == TitanState.GRABBING){
			Vector3 toTarget = this.currentTarget.transform.position - this.transform.position;
			this.rigidBody.velocity = Vector3.Lerp(this.rigidBody.velocity, Vector3.zero, 0.5F);

			if (Vector3.Angle(this.transform.forward, toTarget) < 60F){
				this.head.transform.rotation = Quaternion.Lerp(this.head.transform.rotation, Quaternion.LookRotation(toTarget), this.headRotationSpeed);
			}
			if (this.grabAnimation.isLooping){
				this.state = TitanState.WALKING;
				this.grabAnimation = null;
			}
		}else{
			this.state = TitanState.IDLE;
			this.currentTarget = null;
		}
	}

	public void FixedUpdate(){

	}

	public void LateUpdate(){

	}

    public int tick() {
        return 100;
    }

    public void destroy() {

    }

	public AnimationClip createGrabFor(Vector3 point){
		AnimationClip animation = new AnimationClip();
		//One X,Y,Z curve for each part of the arm, upper,lower and hand respectively
		AnimationCurve[] curves = new AnimationCurve[9];
		GameObject upperArm;
		GameObject lowerArm;
		GameObject hand;
		float orientation = MathUtils.AngleDir(this.transform.position, point); //To the left or right of the titan?
		//Random temp variable.
		float temp;
		for(int i  = 0; i < curves.Length; i++){
			curves[i] = new AnimationCurve();
			curves[i].postWrapMode = WrapMode.Once;
		}
		// )		)        Hand		)     Hand
		// )		)        //			)     //
		// ||    => \      //        => \    //
		// ||		 \\__//				\\  //
		// ||		  	 				 \\//
		if (orientation < 0){
			//left Side
			upperArm = this.leftArmUpper;
			lowerArm = this.leftArmLower;
			hand = this.leftHand;
			//Left arm Goes out a bit, and comes back bit less

			curves[0].AddKey(0F, upperArm.transform.position.x);
			//The left arm always moves left
			temp = point.x - this.transform.position.x;
			temp *= (temp < 0) ? -orientation : orientation;
			curves[0].AddKey(1F, temp);
			curves[0].AddKey(1.5F, temp * 0.3F);

			curves[1].AddKey(0F, upperArm.transform.position.y);
			curves[1].AddKey(1F, (point.y - upperArm.transform.position.y)/2);
			curves[1].AddKey(1.5F, (point.y - upperArm.transform.position.y)/2);

			curves[2].AddKey(0F, upperArm.transform.position.z);
			temp = point.z - upperArm.transform.position.z;
			temp /= 2;
			curves[2].AddKey(1F, temp);
			curves[2].AddKey(1.5F, temp * 0.3F);

			//Lower arm
			curves[3].AddKey(0F, lowerArm.transform.position.x);

			//Hand
			curves[6].AddKey(0F, hand.transform.position.x);
			curves[6].AddKey(1F, hand.transform.position.x);
			curves[6].AddKey(1.5F, hand.transform.position.x);

			curves[7].AddKey(0F, hand.transform.position.y);
			curves[7].AddKey(1F, hand.transform.position.y);
			curves[7].AddKey(1.5F, hand.transform.position.y);

			curves[7].AddKey(0F, hand.transform.position.z);
			curves[7].AddKey(1F, hand.transform.position.z);
			curves[7].AddKey(1.5F, hand.transform.position.z);

			animation.SetCurve("Spine/LeftArmUpper", typeof(Transform), "x", curves[0]);
			animation.SetCurve("Spine/LeftArmUpper", typeof(Transform), "y", curves[1]);
			animation.SetCurve("Spine/LeftArmUpper", typeof(Transform), "z", curves[2]);

			animation.SetCurve("Spine/LeftArmUpper/LeftArmLower", typeof(Transform), "x", curves[3]);
			animation.SetCurve("Spine/LeftArmUpper/LeftArmLower", typeof(Transform), "y", curves[4]);
			animation.SetCurve("Spine/LeftArmUpper/LeftArmLower", typeof(Transform), "z", curves[5]);

			animation.SetCurve("Spine/LeftArmUpper/LeftArmLower/LeftHand", typeof(Transform), "x", curves[6]);
			animation.SetCurve("Spine/LeftArmUpper/LeftArmLower/LeftHand", typeof(Transform), "y", curves[7]);
			animation.SetCurve("Spine/LeftArmUpper/LeftArmLower/LeftHand", typeof(Transform), "z", curves[8]);
		}else{
			//right side
			upperArm = this.rightArmUpper;
			lowerArm = this.rightArmLower;
			hand = this.rightHand;
		}

		return animation;
	}
}

