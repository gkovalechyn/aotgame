using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IKJoint : MonoBehaviour{
	public float xDegreesOfFreedom = 90F;
	public float yDegreesOfFreedom = 90F;
    public float zDegreesOfFreedom = 90F;
	public float weight = 1F;
    public float length = 1F;

	public Rigidbody rigidbody;

    public IKJoint parent;
    public IKJoint child;

	public void Start(){
		this.rigidbody = this.GetComponent<Rigidbody>();
	}

	/// <summary>
	/// Builds the joint list. The joints are always added to the top of the list so that it is inverted,
	/// That way it makes the iteration forwards instead of backwards.
	/// </summary>
	/// <param name="joints">Joints.</param>
	public void buildJointList(LinkedList<IKJoint> joints){
		this.child = this.GetComponentInChildren<IKJoint>();

        if (joints.Count > 0){
            this.parent = joints.First.Value;
        } else {
            this.parent = null;
        }

		joints.AddFirst(this);
		this.child.buildJointList(joints);
	}
}

