using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IKAnimator : IKJoint{
	private LinkedList<IKJoint> joints = new LinkedList<IKJoint>();
	private bool isAnimating = false;
	public const float IK_DEFAULT_PRECISION = 0.1F;

	#region Current animation fields
	private Vector3 target;
	private float precision;
	private onReachPosition onReach;
	#endregion

	public bool IsAnimating{
		get{
			return this.isAnimating;
		}
		set{
			this.isAnimating = value;
		}
	}

	public delegate void onReachPosition();

	public void Start(){
		this.buildJointList(this.joints);
	}

	public void reachPoint(Vector3 pos){
        this.reachPoint(pos, IK_DEFAULT_PRECISION);
	}

	public void reachPoint(Vector3 pos, float precision){
        this.reachPoint(pos, precision, null);
	}

	public void reachPoint(Vector3 position, float precision, onReachPosition onReach){
        this.target = position;
        this.precision = precision;
        this.onReach = onReach;

        this.isAnimating = true;
	}

	public void cancelAnimation(){
		this.isAnimating = false;
		this.precision = IKAnimator.IK_DEFAULT_PRECISION;
		this.onReach = null;
	}

	public void FixedUpdate(){
		if (this.isAnimating){
			Transform endEffector = this.joints.First.Value.transform;
			Vector3 toTheEndEffector = Vector3.zero;
			Vector3 endEffectorToTarget = Vector3.zero;
			float cos;

			//The list should be inverted so the first iteration should be the last element.
			foreach(IKJoint joint in this.joints){
				if (this.hasReached()){
					if (this.onReach != null){
						this.onReach();
					}
					this.cancelAnimation();
					return;
				}

				toTheEndEffector = (joint.transform.position - endEffector.position).normalized;
				endEffectorToTarget = (endEffector.position - this.target).normalized;
				cos = Vector3.Dot(toTheEndEffector, endEffectorToTarget);

				if(cos < 0.999F){
					Vector3 cross = Vector3.Cross(toTheEndEffector, endEffectorToTarget);

					if (cross.z > 0){

					}else if(cross.z < 0){
                        
					}
				}
			}
		}
	}

	private bool hasReached(){
		return ((this.joints.First.Value.transform.position - this.target).sqrMagnitude < this.precision);
	}
}