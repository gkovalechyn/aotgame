using UnityEngine;
using System.Collections;
using Assets.Scripts.AI;

public interface ITitanAI{
	/// <summary>
	/// Tick this instance.
	/// <returns>The current threat level of this titan, preferably clamped between 0 and 100</returns>
	/// </summary>
	int tick();

	/// <summary>
	/// Destroy this instance. Called by the AIManager in case something wrong happens with this titan.
	/// </summary>
	void destroy();

    GameObject getBodyPart(BodyPart part);
}

