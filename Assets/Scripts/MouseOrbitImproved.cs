﻿using UnityEngine;
using System.Collections;

public class MouseOrbitImproved : MonoBehaviour {
	
	public Transform target;
	public float distance = 5f;

	public float xSpeed = 120.0f;
	public float ySpeed = 120.0f;

	public Vector3 offset = new Vector3(0f, 5f, 0f);
	public Vector3 lookOffset = new Vector3(0f, 0.5f, 0f);
	public float rotationOffset = 3f;

	public float distanceMin = 1f;
	public float distanceMax = 15f;
	public float distanceChange = 1f;

	private bool hitSomethingLastFrame = false;
	private Vector3 lastHitPoint;
	private Vector3 whereItShouldBe;
	public GameObject cameraDummy = null;

	//object used to rotate the camera after it's position has been set
	private GameObject cameraRotationObject;

	private bool lockCursor = false;

	private Vector3 oldTargetPosition;

	public LayerMask raycastBypassLayer;
	// Use this for initialization

	void Start () {
		if (this.target == null){
			throw new System.Exception("Target is null.");
		}

		if (this.cameraDummy == null){
			this.cameraDummy = new GameObject();
		}

		this.init();
	}

	public void init(){
		Vector3 angles = transform.eulerAngles;

		cameraRotationObject = new GameObject();

		// Make the rigid body not change rotation
		if (GetComponent<Rigidbody>())
			GetComponent<Rigidbody>().freezeRotation = true;

		//set the position without the rotation
		whereItShouldBe = (-target.transform.forward).normalized * distance + target.transform.position;
		cameraDummy.transform.position = whereItShouldBe;
		//rotate it and then set it back

		oldTargetPosition = target.transform.position;
	}
	void Update(){
		if (Input.GetKeyDown(KeyCode.P)){
			this.lockCursor = !lockCursor;
			Cursor.visible = !this.lockCursor;
			Cursor.lockState = this.lockCursor ? CursorLockMode.Locked : CursorLockMode.None;
		}

		if (Input.GetKeyDown(KeyCode.K)){
			if (this.distance < this.distanceMax){
				this.distance += this.distanceChange;
			}
		}

		if (Input.GetKeyDown(KeyCode.L)){
			if (this.distance > this.distanceMin){
				this.distance -= this.distanceChange;
			}
		}
	}

	void LateUpdate(){
		float x = Input.GetAxis("Mouse X") * xSpeed * distance * Time.deltaTime * Options.mouseSensitivityX;
		float y = Input.GetAxis("Mouse Y") * ySpeed * Time.deltaTime * Options.mouseSensitivityY;
		Vector3 finalPosition;
		RaycastHit hit;
		Vector3 targetOffset = this.target.transform.position - this.oldTargetPosition;
		Vector3 dummyOffset;
		Vector3 lookOffset = this.lookOffset;
		//this.cameraDummy.transform.position = this.target.transform.position + dummyOffset.normalized * this.distance;

		//translate the camera based on what the target moved
		this.cameraDummy.transform.position += targetOffset;

		//rotate the camera around the player based on what the mouse moved
		this.cameraDummy.transform.RotateAround(this.target.transform.position, Vector3.up, x);
		this.cameraDummy.transform.RotateAround(this.target.transform.position, -this.transform.right, y);

		//fix the distance to the camera by changing the position of the dummy
		dummyOffset = this.cameraDummy.transform.position - this.target.transform.position;
		cameraDummy.transform.position = target.transform.position + dummyOffset.normalized * distance;

		this.whereItShouldBe = cameraDummy.transform.position;

		if (Physics.Raycast(new Ray(target.transform.position, whereItShouldBe - this.target.transform.position), out hit, this.distance)){
			finalPosition = hit.point;
			hitSomethingLastFrame = true;
			this.lastHitPoint = finalPosition;
			lookOffset = new Vector3(this.lookOffset.x, Vector3.Distance(finalPosition, this.target.transform.position), this.lookOffset.z);
		}else{
			finalPosition = whereItShouldBe;
		}

		if (!this.hitSomethingLastFrame){
			finalPosition += this.offset;
			//add the rotation offset to the camera final position
			cameraRotationObject.transform.position = finalPosition;
			cameraRotationObject.transform.RotateAround(this.target.transform.position, Vector3.up, this.rotationOffset);

			finalPosition = cameraRotationObject.transform.position;
		}

		//set the camera in the right location and make it look at the right place
		this.transform.position = finalPosition;
		//look at the position based on local space and not global space
		//Vector3 localTargetSpace = this.target.transform.InverseTransformPoint(this.lookOffset);
		this.transform.LookAt(this.target.position + lookOffset);

		this.oldTargetPosition = this.target.transform.position;
	}	
	
}
