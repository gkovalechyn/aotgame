﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

public class FileChooserController : MonoBehaviour {
	private string currentPath;
	private onOk onOkMethod;
	private onCancel onCancelMethod;

	private int displayFrom;
	private int displayTo;
	private int itemsToDisplay = 5;

	private ItemInfo[] itemsInDirectory;
	private IDictionary<int, long> clickTimes = new Dictionary<int, long>();

	private UnityEngine.Object ItemInfoPrefab;

	public Text fileNameInput;
	public Scrollbar scrollbar;
	public Transform mainPanelTransform;

	public Texture2D directoryTexture;
	public Texture2D fileTexture;


	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void show(string dir, onOk onOkMethod, onCancel onCancelMethod){
		this.onOkMethod = onOkMethod;
		this.onCancelMethod = onCancelMethod;
		Debug.Log("Directory recieved: " + dir);
		this.currentPath = Application.absoluteURL + dir;
		Debug.Log("Full path" + this.currentPath);
		this.ItemInfoPrefab = Resources.Load("UI/FileDescription");

		this.loadItemsInDir();

		this.transform.parent.gameObject.SetActive(true);

		this.displayFrom = 0;
		this.displayTo = Mathf.Min(this.itemsInDirectory.Length, itemsToDisplay);

		this.scrollbar.size = Mathf.Min(1F, this.itemsToDisplay / (float) this.itemsInDirectory.Length);
		this.scrollbar.value = 0F;

		this.updateVisibleObjects();
	}

	public void onOkClicked(){
		string text = this.fileNameInput.text;

		this.onOkMethod(this.currentPath + text);

		this.destroy();
	}

	public void onCancelClicked(){
		this.onCancelMethod();

		this.destroy();
	}

	public void onScrollbarValueChanged(){
		float value = this.scrollbar.value;
		int min = Mathf.Max(0, (int) (value * this.itemsInDirectory.Length));
		int max = Mathf.Min(this.itemsInDirectory.Length, min + this.itemsToDisplay);

		if (min != this.displayFrom || max != this.displayTo){
			this.displayFrom = min;
			this.displayTo = max;
			this.updateVisibleObjects();
		}
	}

	private void destroy(){
		this.transform.parent.gameObject.SetActive(false);
		GameObject.Destroy(this.transform.parent.gameObject);
	}

	private void loadItemsInDir(){
		string[] dirs = Directory.GetDirectories(this.currentPath);
		string[] files = Directory.GetFiles(this.currentPath);
		int index = 1;

		if (this.itemsInDirectory != null){
			foreach(ItemInfo info in this.itemsInDirectory){
				GameObject.Destroy(info.getItem());
			}
		}

		this.itemsInDirectory = new ItemInfo[dirs.Length + files.Length + 1];
		this.clickTimes.Clear();

		GameObject backItem = (GameObject)GameObject.Instantiate(this.ItemInfoPrefab);
		backItem.transform.SetParent(this.mainPanelTransform);
		FileDescriptionController desc = backItem.GetComponentInChildren<FileDescriptionController>();
		desc.setParentController(this);
		desc.setId(0);
		this.itemsInDirectory[0] = new ItemInfo(this.directoryTexture, "../", backItem, true);
		this.itemsInDirectory[0].update();

		for(int i = 0; i < dirs.Length; i++){
			GameObject go = (GameObject)GameObject.Instantiate(this.ItemInfoPrefab);
			go.transform.SetParent(this.mainPanelTransform);
			desc = backItem.GetComponentInChildren<FileDescriptionController>();
			desc.setParentController(this);
			desc.setId(index);
			this.itemsInDirectory[index] = new ItemInfo(this.directoryTexture, dirs[i], go, true);
			this.itemsInDirectory[index].update();
			index++;
		}

		for(int i = 0; i < files.Length; i++){
			GameObject go = (GameObject)GameObject.Instantiate(this.ItemInfoPrefab);
			go.transform.SetParent(this.mainPanelTransform);
			desc = backItem.GetComponentInChildren<FileDescriptionController>();
			desc.setParentController(this);
			desc.setId(index);
			this.itemsInDirectory[index] = new ItemInfo(this.fileTexture, files[i], backItem, true);
			this.itemsInDirectory[index].update();

			Debug.Log("Created item for file: " + files[i]);

			index++;
		}
	}

	public void itemRecievedClick(int itemID){
		ItemInfo info = this.itemsInDirectory[itemID];

		if (info.isDir()){
			if (itemID == 0){
				int count = 1;
				for(int i = this.currentPath.Length - 2; i >= 0; i++){
					if (this.currentPath[i] == '/' || this.currentPath[i] == '\\'){
						break;
					}
					count++;
				}
				this.currentPath = this.currentPath.Substring(0, this.currentPath.Length - count);
			}else{
				this.currentPath += info.getText() + "/";
			}

			this.loadItemsInDir();
		}else{
			this.fileNameInput.text = info.getText();
		}
		/*
		ItemInfo info = this.itemsInDirectory[itemID];
		long lastPress;

		try{
			lastPress = this.clickTimes[itemID];
		}catch(System.Exception){
			this.clickTimes[itemID] = DateTime.Now.Ticks;
			return;
		}

		if (DateTime.Now.Ticks - lastPress < 100){
			this.clickTimes[itemID] = DateTime.Now.Ticks;
		}
		*/
	}

	private void updateVisibleObjects(){
		for(int i = 0; i < this.itemsInDirectory.Length; i++){
			ItemInfo info = this.itemsInDirectory[i];

			if (i >= this.displayFrom && i < this.displayTo){
				info.getItem().SetActive(true);
			}else{
				info.getItem().SetActive(false);
			}
		}
	}

	private class ItemInfo{
		private Texture2D texture;
		private string text;
		private GameObject itemInfo;
		private bool dir;

		public ItemInfo (UnityEngine.Texture2D texture, string text, UnityEngine.GameObject itemInfo, bool isDir){
			this.texture = texture;
			this.text = text;
			this.itemInfo = itemInfo;
			this.dir = isDir;
		}

		public Texture2D getTexture(){
			return texture;
		}

		public string getText(){
			return this.text;
		}

		public GameObject getItem(){
			return this.itemInfo;
		}

		public bool isDir(){
			return this.dir;
		}

		public void update(){
			this.itemInfo.GetComponent<Image>().sprite = Sprite.Create(texture, new Rect(0,0,32,32), Vector2.zero);
			this.itemInfo.GetComponentInChildren<Text>().text = this.text;
		}
	}
	public delegate void onOk(string path);
	public delegate void onCancel();
}