﻿using UnityEngine;
using System.Collections;

public class RotatingObject : MonoBehaviour {
	public Vector3 rotateAround = Vector3.up;
	public Vector3 point;
	public float ratio = 0.1f;
	public bool staticPoint = false;

	// Use this for initialization
	void Start () {
	}

	void FixedUpdate () {
		this.transform.RotateAround(staticPoint ? point : this.transform.position,
		                            rotateAround, ratio);
	}
}
