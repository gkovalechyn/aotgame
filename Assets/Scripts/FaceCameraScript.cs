using UnityEngine;
using System.Collections;

public class FaceCameraScript : MonoBehaviour{
	public Camera cameraToFace;
	// Use this for initialization
	void Start (){
		if (cameraToFace == null){
			cameraToFace = Camera.main;
		}
	}

	// Update is called once per frame
	void Update (){
		this.transform.rotation = Quaternion.LookRotation((cameraToFace.transform.position - this.transform.position));
	}
}

